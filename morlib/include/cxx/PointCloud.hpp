/*
 * Copyright(c) 2014 - 2015 Krzysztof Jordan
 * Stevens Institute of Technology
 * kjordan1 at stevens edu
 */

#pragma once

#include <armadillo>
#include <memory>
#include <vector>

#include <stdmor.h>

#include "Point.hpp"
#include "Plane.hpp"

MORLIB_NAMESPACE

struct Point;
class ISensor;

struct PlaneFilteringResult
{
	Plane plane;
	Point planeCorners[4];
	std::vector<Point> inliers;
	Point center;
	float width;
	float height;
};

struct PlaneFilteringParams
{
	arma::uword maxFilteredPoints;
	unsigned int maxNeighborhoods;
	arma::uword inlierPreallocate;
	arma::fmat* rawPlaneNormals;
	arma::uvec* rawInlierCount;
	arma::umat* rawInlierIndicies;
	arma::vec* performanceResults;

	PlaneFilteringParams() : 
		maxFilteredPoints(2000),
		maxNeighborhoods(20000),
		inlierPreallocate(300000),
		rawPlaneNormals(nullptr),
		rawInlierCount(nullptr),
		rawInlierIndicies(nullptr),
		performanceResults(nullptr)
	{
	}
};

class PointCloud
{
public:
	PointCloud(const arma::fmat& depthMatrix, const std::shared_ptr<ISensor>& ptr, bool computeAtStart = true);
	
	void recomputePointCloud(const arma::fmat& depthMatrix);

	// col = x, row = y
	int getIndex(int col, int row) const;
	const Point& getPoint(int index) const;
	const std::vector<Point>& getPoints() const;

	Point getCenter() const;
	const Point& getMinBound() const;
	const Point& getMaxBound() const;

	arma::uword getWidth() const;
	arma::uword getHeight() const;

	void writeToFile(const std::string& name) const;

	Plane fitPlane(float distanceThreshold = 0.05f) const;
	std::vector<PlaneFilteringResult> planeFiltering(arma::sword neighborhood_range, float plane_size, float s, float t,
		const PlaneFilteringParams& params = PlaneFilteringParams()) const;
private:
	unsigned int _imageSeed;
	std::vector<Point> _points;
	arma::uword _width;
	arma::uword _height;
	std::shared_ptr<ISensor> _sensorPtr;
	Point _min;
	Point _max;
};

MORLIB_NAMESPACE_END

#include "impl/PointCloud_impl.hpp"
