/*
 * Copyright(c) 2014 - 2015 Krzysztof Jordan
 * Stevens Institute of Technology
 * kjordan1 at stevens edu
 */

#pragma once

#include <armadillo>

#include <stdmor.h>

MORLIB_NAMESPACE

class ISensor;

struct Point
{
	float x;
	float y;
	float z;

	Point(float value = 0.0f);
	Point(float x, float y, float z);
	Point(const arma::subview_row<float>& vec);
	Point(const ISensor* sensor, arma::uword x, arma::uword y, arma::uword width, arma::uword height, float depth);
	Point(const ISensor* sensor, arma::uword x, arma::uword y, arma::uword width, arma::uword height, float depth, float th, float tv);
};

MORLIB_NAMESPACE_END

#include "impl/Point_impl.hpp"
