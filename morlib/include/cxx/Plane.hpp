/*
 * Copyright(c) 2014 - 2015 Krzysztof Jordan
 * Stevens Institute of Technology
 * kjordan1 at stevens edu
 */

#pragma once

#include <stdmor.h>

MORLIB_NAMESPACE

struct Point;

struct Plane
{
	float a;
	float b;
	float c;
	float d;

	// Define a plane  ax + by + cz + d = 0
	Plane(float a, float b, float c, float d) :
		a(a), b(b), c(c), d(d)
	{
	}

	inline const Point& getNormal() const
	{
		return *(reinterpret_cast<const Point*>(&this->a));
	}
};

MORLIB_NAMESPACE_END
