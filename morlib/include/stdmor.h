/*
 * Copyright(c) 2014 - 2015 Vincent Simonetti
 * Stevens Institute of Technology
 * vsimonet at stevens edu
 */

#ifndef STD_MORLIB_H
#define STD_MORLIB_H

#if defined(_WIN32) || defined(__CYGWIN__)
	#ifdef MORLIB_EXPORTS
		#ifdef __GNUC__
			#define MORLIB_API __attribute__ ((dllexport))
		#else
			#define MORLIB_API __declspec(dllexport)
		#endif
	#else
		#ifdef MORLIB_STATIC_BUILD
			#define MORLIB_API
		#else
			#ifdef __GNUC__
				#define MORLIB_API __attribute__ ((dllimport))
			#else
				#define MORLIB_API __declspec(dllimport)
			#endif
		#endif
	#endif
#else
	#if __GNUC__ >= 4
		#define MORLIB_API __attribute__ ((visibility ("default")))
	#else
		#define MORLIB_API
	#endif
#endif

#ifdef MORLIB_NO_NAMESPACES
	#define MORLIB_NAMESPACE
	#define MORLIB_NAMESPACE_END
	#define MORLIB_NAMESPACE_PREFIX
	#define MORLIB_NAMESPACE_USE
#else
	#define MORLIB_NAMESPACE namespace mor {
	#define MORLIB_NAMESPACE_END }
	#define MORLIB_NAMESPACE_PREFIX mor::
	#define MORLIB_NAMESPACE_USE using namespace mor
#endif

MORLIB_NAMESPACE

typedef arma::fmat (*fitting_func)(const arma::fmat& x);
typedef arma::uvec (*distance_func)(arma::fmat& M_out, const arma::fmat& M_in, const arma::fmat& x, float t);

MORLIB_NAMESPACE_END

#endif