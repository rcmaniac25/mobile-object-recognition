/*
 * Copyright(c) 2014 - 2015 Vincent Simonetti
 * Stevens Institute of Technology
 * vsimonet at stevens edu
 *
 * Copyright(c) 2014 - 2015 Krzysztof Jordan
 * Stevens Institute of Technology
 * kjordan1 at stevens edu
 *
 * --------------------------------------------------------------
 *
 * "random_sample", "is_colinear"
 *
 * Copyright(c) 2006 Peter Kovesi
 * School of Computer Science & Software Engineering
 * The University of Western Australia
 * pk at csse uwa edu au
 * http://www.csse.uwa.edu.au/~pk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files(the "Software"), to deal
 * in the Software without restriction, subject to the following conditions :
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * The Software is provided "as is", without warranty of any kind.
 */

#ifndef MORLIB_UTIL_H
#define MORLIB_UTIL_H

#include <armadillo>
#include <stdmor.h>

MORLIB_NAMESPACE

#define length(X) std::max(X.n_cols, X.n_rows)
#define mod(X, M) (X - (floor(X / M) * M))
#define modu(X, M) (X - ((X / M) * M))
#define fast_round_uword(x) static_cast<arma::uword>(std::floorf(x) + 0.5f)

enum depth_convert_settings
{
	DEPTH_CONVERT_ALL,
	DEPTH_CONVERT_ALL_NEGATIVE_SET_DEPTH,
	DEPTH_CONVERT_ALL_ZERO_NEGATIVE_SET_DEPTH,
	DEPTH_CONVERT_IGNORE_NEGATIVE,
	DEPTH_CONVERT_IGNORE_ZERO_NEGATIVE
};

// Returns a value between 0 and 1.
inline float matlab_randf()
{
	return rand() / float(RAND_MAX);
}

inline double matlab_rand()
{
	return rand() / double(RAND_MAX);
}

// Returns a value between 0 and max
inline float rand_mf(float max)
{
	return matlab_randf() * max;
}

inline double rand_m(double max)
{
	return matlab_rand() * max;
}

// Returns a value between min and max
inline float rand_mmf(float min, float max)
{
	return (matlab_randf() * (max - min)) + min;
}

inline double rand_mm(double min, double max)
{
	return (matlab_rand() * (max - min)) + min;
}

/*
 * Creates a random seed based on contents of matrix.
 *
 * @param x The matrix to generate a seed for.
 * @return Generated seed.
 */
MORLIB_API unsigned int generate_seed(const arma::fmat& x);

/*
 * Generate a vector of elements
 *
 * @param start The value of the first element in the vector.
 * @param end The last element of the vector.
 * @param inc The increment between values.
 * @return Vector filled with the specified values.
 */
template<typename eT>
arma_hot arma_inline arma::Col<eT> gen_elements(eT start, eT end, eT inc = eT(1))
{
	// If linspace could set an already allocated matrix, then we would use that.
	arma_extra_debug_sigprint();

	arma::Col<eT> ret(static_cast<arma::uword>(std::floor(std::abs(static_cast<double>((end - start) / inc)))) + 1);

	eT val = start;
	eT* ptr = ret.memptr();
	for (arma::uword i = 0; (inc > 0) ? (val < end) : (val > end); i++, val += inc) { ptr[i] = val; }

	return ret;
}

/*
 * Fill a matrix with the index value of the element.
 *
 * @param mat The matrix to fill.
 * @return Matrix filled with the index values.
 */
template<typename eT>
arma_hot arma_inline const arma::Mat<eT>& fill_index(arma::Mat<eT>& mat)
{
	// If linspace could set an already allocated matrix, then we would use that.
	arma_extra_debug_sigprint();

	eT val = eT(0);
	eT* ptr = mat.memptr();
	for (arma::uword i = 0; i < mat.n_elem; i++, val++) { ptr[i] = val; }

	return mat;
}

/*
 * Unique row in matrix.
 *
 * @param x The matrix to find unique rows of.
 * @return The unique rows of the matrix, in sorted order.
 */
template<typename eT>
arma_hot arma_inline arma::Mat<eT> unique_rows(const arma::Mat<eT>& x)
{
	arma::Mat<eT> out = sort(x);

	for (arma::uword i = 1; i < out.n_rows; i++)
	{
		const arma::Row<eT> a = out.row(i - 1);
		const arma::Row<eT> b = out.row(i);
		if (all(a == b))
		{
			out.shed_row(i--);
		}
	}

	return out;
}

/*
 * Unique columns in matrix.
 *
 * @param x The matrix to find unique columns of.
 * @return The unique columns of the matrix, in sorted order.
 */
template<typename eT>
arma_hot arma_inline arma::Mat<eT> unique_cols(const arma::Mat<eT>& x)
{
	arma::Mat<eT> out = sort(x, "ascend", 1);

	for (arma::uword i = 1; i < out.n_cols; i++)
	{
		const arma::Col<eT> a = out.col(i - 1);
		const arma::Col<eT> b = out.col(i);
		if (all(a == b))
		{
			out.shed_col(i--);
		}
	}

	return out;
}

/*
 * Remove an arbitrary array of row indicies.
 *
 * @param x The matrix to shed rows from.
 * @param r The row indicies to remove.
 */
template<typename eT>
arma_hot arma_inline void shed_rows(arma::Mat<eT>& x, const arma::uvec& r)
{
	arma_extra_debug_sigprint();

	arma_debug_check
		(
		r.n_rows != 1 || r.n_cols != 1,
		"shed_rows(): must be one row or column"
		);

	if (r.n_elem == 1)
	{
		x.shed_row(r[0]);
	}
	else
	{
		arma::uvec sr = unique(r);
		if (sr.n_elem == x.n_rows)
		{
			x.reset();
		}
		else
		{
			arma::uword r1 = sr[0];
			arma::uword r2 = r1;
			bool has_run = false;
			arma::uword last_shed = 0;
			for (arma::uword i = 1; i < sr.n_elem; i++)
			{
				arma::uword r = sr[i];
				if ((r2 + 1) != r)
				{
					x.shed_rows(r1, r2);
					last_shed = r2;
					r1 = r2 = r;
					has_run = true;
				}
				else
				{
					r2 = r;
				}
			}
			if (last_shed < r1 || !has_run)
			{
				x.shed_rows(r1, r2);
			}
		}
	}
}

/*
 * Remove an arbitrary array of col indicies.
 *
 * @param x The matrix to shed cols from.
 * @param r The col indicies to remove.
 */
template<typename eT>
arma_hot arma_inline void shed_cols(arma::Mat<eT>& x, const arma::uvec& r)
{
	arma_extra_debug_sigprint();

	arma_debug_check
		(
		r.n_rows != 1 || r.n_cols != 1,
		"shed_cols(): must be one row or column"
		);

	if (r.n_elem == 1)
	{
		x.shed_col(r[0]);
	}
	else
	{
		arma::uvec sr = unique(r);
		if (sr.n_elem == x.n_cols)
		{
			x.reset();
		}
		else
		{
			arma::uword r1 = sr[0];
			arma::uword r2 = r1;
			bool has_run = false;
			arma::uword last_shed = 0;
			for (arma::uword i = 1; i < sr.n_elem; i++)
			{
				arma::uword r = sr[i];
				if ((r2 + 1) != r)
				{
					x.shed_cols(r1, r2);
					last_shed = r2;
					r1 = r2 = r;
					has_run = true;
				}
				else
				{
					r2 = r;
				}
			}
			if (last_shed < r1 || !has_run)
			{
				x.shed_cols(r1, r2);
			}
		}
	}
}

/*
 * Selects n random items from an array.
 *
 * @param a An integer describing the size of an array to pick values from.
 * @param n The number of items to be selected.
 * @return Column vector of K values sampled uniformly at random, without replacement.
 */
MORLIB_API arma::umat random_sample(arma::uword a, arma::uword n);
/*
 * Selects n random items from an array.
 *
 * @param a An array of values from which the items are to be selected.
 * @param n The number of items to be selected.
 * @return Column vector of K values sampled uniformly at random, without replacement.
 */
MORLIB_API arma::umat random_sample(arma::umat& a, arma::uword n);

/*
 * Select columns based on a vector of indicies. 
 *
 * @param p1 Point in 2D or 3D.
 * @param p2 Point in 2D or 3D.
 * @param p3 Point in 2D or 3D.
 * @param homogneeous Flag indicating that p1, p2, p3 are homogneeous coordinates with arbitrary scale. If this is omitted it is assumed that the points are 
 *	inhomogeneous, or that they are homogeneous with equal scale.
 * @return true if the points are co-linear, false if otherwise.
 */
MORLIB_API bool is_colinear(const arma::fvec& p1, const arma::fvec& p2, const arma::fvec& p3, bool homogneeous = false);

/*
 * Convert a depth sub-image into a point cloud.
 *
 * @param i The depth image to convert.
 * @param x The horizontal offset into the depth image.
 * @param y The vertical offset into the depth image.
 * @param w The width of the sub-section of the depth image to convert.
 * @param h The height of the sub-section of the depth image to convert.
 * @param fh The horizontal field of view of the depth image.
 * @param fv The vertical field of view of the depth image.
 * @param settings Conversion settings.
 * @param actual_indicies If settings is not DEPTH_CONVERT_ALL, then this will provide the actual indicies for the rows. Otherwise this will be no different then a simple for loop.
 * @param abs_indicies Only used if actual_indicies is set. If true, actual_indicies will specify indicies for 'i'. If false (default), then it will specify indicies for the sub-image of 'i'.
 * @return The point cloud of the depth image.
 */
MORLIB_API arma::fmat depth_sub_image_to_point_cloud(const arma::fmat& i, arma::uword x, arma::uword y, arma::uword w, arma::uword h, float fh, float fv,
	depth_convert_settings settings = DEPTH_CONVERT_ALL, arma::uvec* actual_indicies = NULL, bool abs_indicies = false);

/*
 * Convert a depth image into a point cloud.
 *
 * @param i The depth image to convert.
 * @param fh The horizontal field of view of the depth image.
 * @param fv The vertical field of view of the depth image.
 * @param settings Conversion settings.
 * @param actual_indicies If settings is not DEPTH_CONVERT_ALL, then this will provide the actual indicies for the rows. Otherwise this will be no different then a simple for loop.
 * @param abs_indicies Only used if actual_indicies is set. If true, actual_indicies will specify indicies for 'i'. If false (default), then it will specify indicies for the sub-image of 'i'.
 * @return The point cloud of the depth image.
 */
inline arma::fmat depth_image_to_point_cloud(const arma::fmat& i, float fh, float fv,
	depth_convert_settings settings = DEPTH_CONVERT_ALL, arma::uvec* actual_indicies = NULL, bool abs_indicies = false)
{
	return depth_sub_image_to_point_cloud(i, 0, 0, i.n_cols, i.n_rows, fh, fv, settings, actual_indicies, abs_indicies);
}

/*
 * Convert a depth sub-image into a point cloud.
 *
 * @param i The depth image to convert.
 * @param x The horizontal offset into the depth image.
 * @param y The vertical offset into the depth image.
 * @param w The width of the sub-section of the depth image to convert.
 * @param h The height of the sub-section of the depth image to convert.
 * @param fl The focal length of the depth image.
 * @param settings Conversion settings.
 * @param actual_indicies If settings is not DEPTH_CONVERT_ALL, then this will provide the actual indicies for the rows. Otherwise this will be no different then a simple for loop.
 * @param abs_indicies Only used if actual_indicies is set. If true, actual_indicies will specify indicies for 'i'. If false (default), then it will specify indicies for the sub-image of 'i'.
 * @return The point cloud of the depth image.
 */
MORLIB_API arma::fmat depth_sub_image_to_point_cloud(const arma::fmat& i, arma::uword x, arma::uword y, arma::uword w, arma::uword h, float fl,
	depth_convert_settings settings = DEPTH_CONVERT_ALL, arma::uvec* actual_indicies = NULL, bool abs_indicies = false);

/*
 * Convert a depth sub-image into a point cloud.
 *
 * @param i The depth image to convert.
 * @param fl The focal length of the depth image.
 * @param settings Conversion settings.
 * @param actual_indicies If settings is not DEPTH_CONVERT_ALL, then this will provide the actual indicies for the rows. Otherwise this will be no different then a simple for loop.
 * @param abs_indicies Only used if actual_indicies is set. If true, actual_indicies will specify indicies for 'i'. If false (default), then it will specify indicies for the sub-image of 'i'.
 * @return The point cloud of the depth image.
 */
inline arma::fmat depth_image_to_point_cloud(const arma::fmat& i, float fl,
	depth_convert_settings settings = DEPTH_CONVERT_ALL, arma::uvec* actual_indicies = NULL, bool abs_indicies = false)
{
	return depth_sub_image_to_point_cloud(i, 0, 0, i.n_cols, i.n_rows, fl, settings, actual_indicies, abs_indicies);
}

/*
 * Convert a depth value to a 3D point.
 *
 * @param depth The depth value to convert.
 * @param x The horizontal position of the depth value.
 * @param y The vertical position of the depth value.
 * @param w The width of the surface the depth value originated from.
 * @param h The height of the surface the depth value originated from.
 * @param fh The horizontal field of view of the depth surface.
 * @param fv The vertical field of view of the depth surface.
 * @return The 3D position of the depth value.
 */
inline arma::frowvec depth_to_point(float depth, arma::uword x, arma::uword y, arma::uword w, arma::uword h, float fh, float fv)
{
	arma::frowvec3 res = {
		depth * (((float)x / (w - 1)) - 0.5f) * tanf(fh / 2.0f),
		depth * (((float)y / (h - 1)) - 0.5f) * tanf(fv / 2.0f),
		depth };
	return res;
}

/*
 * Convert a depth value to a 3D point.
 *
 * @param depth The depth value to convert.
 * @param x The horizontal position of the depth value.
 * @param y The vertical position of the depth value.
 * @param fl The focal length of the depth surface.
 * @return The 3D position of the depth value.
 */
inline arma::frowvec depth_to_point(float depth, arma::uword x, arma::uword y, float fl)
{
	auto invDepth = depth / fl;
	arma::frowvec3 res = {
		invDepth * x,
		invDepth * y,
		depth };
	return res;
}

/*
 * Extract a subset of a image-based-point cloud.
 *
 * @param XYZ Point cloud.
 * @param XYZ_width Source image width for point cloud.
 * @param XYZ_height Source image height for point cloud.
 * @param x The horizontal offset into the point cloud.
 * @param y The vertical offset into the point cloud.
 * @param w The width of the sub-section of the point cloud to extract.
 * @param h The height of the sub-section of the point cloud to extract.
 * @param settings Conversion settings.
 * @param actual_indicies If settings is not DEPTH_CONVERT_ALL, then this will provide the actual indicies for the rows. Otherwise this will be no different then a simple for loop.
 * @return The subset of the point cloud.
 */
MORLIB_API arma::fmat point_cloud_sub_window(const arma::fmat& XYZ, arma::uword XYZ_width, arma::uword XYZ_height, arma::uword x, arma::uword y, arma::uword w, arma::uword h,
	depth_convert_settings settings = DEPTH_CONVERT_ALL, arma::uvec* actual_indicies = NULL);

/*
 * Write a set of points to a file in PLY format.
 *
 * @param file The file to write to.
 * @param XYZ The points to write.
 * @param colors The colors for each point.
 */
MORLIB_API void write_ply(const char* file, const arma::fmat& XYZ, const arma::umat& colors = arma::umat());

/*
 * Copy a pointer to a fmat.
 *
 * @param data The data pointer that should contain floating point types at the most basic level.
 * @param width The width of the matrix.
 * @param height The height of the matrix.
 * @param transpose Transpose the matrix before returning.
 * @return The converted matrix.
 */
template<typename eT>
inline arma::fmat clone_fmat_from_pointer(const eT* data, arma::uword width, arma::uword height, bool transpose = false)
{
	fmat m(reinterpret_cast<const float*>(data), width, height);
	return transpose ? m : m.t();
}

/*
 * Copy a STL vector to a fmat.
 *
 * @param v The input vector that should contain floating point types at the most basic level.
 * @param width The width of the matrix.
 * @param height The height of the matrix.
 * @param transpose Transpose the matrix before returning.
 * @return The converted matrix.
 */
template<typename eT>
inline const arma::fmat clone_fmat_from_vector(const std::vector<eT>& v, arma::uword width, arma::uword height, bool transpose = false)
{
	// could potentially use conv_to<fmat>::from(v), but then need additional work for wht width, height, and transpose
	return clone_fmat_from_pointer<eT>(v.data(), width, height, transpose);
}

/*
 * Convert a pointer to a fmat.
 *
 * @param data The data pointer that should contain floating point types at the most basic level.
 * @param width The width of the matrix.
 * @param height The height of the matrix.
 * @param transpose Transpose the matrix before returning.
 * @return The converted matrix.
 */
template<typename eT>
inline const arma::fmat create_fmat_from_pointer(const eT* data, arma::uword width, arma::uword height, bool transpose = false)
{
	// CAREFUL!!! This takes the data, in a const context, and un-consts it so it can be set directly to fmat without any memory copying.
	// AKA, it's very fast but if you screw something up, you're a bad person... fmat won't let you, but between 'data' and the next line... who knows...
	fmat m(const_cast<float*>(reinterpret_cast<const float*>(data)), width, height, false, true);
	return transpose ? m : m.t();
}

/*
 * Convert a STL vector to a fmat.
 * 
 * @param v The input vector that should contain floating point types at the most basic level.
 * @param width The width of the matrix.
 * @param height The height of the matrix.
 * @param transpose Transpose the matrix before returning.
 * @return The converted matrix.
 */
template<typename eT>
inline const arma::fmat create_fmat_from_vector(const std::vector<eT>& v, arma::uword width, arma::uword height, bool transpose = false)
{
	return create_fmat_from_pointer<eT>(v.data(), width, height, transpose);
}

/*
 * A general purpose counter's value in milliseconds.
 * 
 * @return a counter's value in milliseconds.
 */
MORLIB_API double counterMS();

MORLIB_NAMESPACE_END

#endif
