/*
 * Copyright(c) 2014 - 2015 Vincent Simonetti
 * Stevens Institute of Technology
 * vsimonet at stevens edu
 *
 * --------------------------------------------------------------
 *
 * For: "mor_ransac"
 *
 * Copyright(c) 2003 - 2013 Peter Kovesi
 * Centre for Exploration Targeting
 * The University of Western Australia
 * peter.kovesi at uwa edu au
 * http://www.csse.uwa.edu.au/~pk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files(the "Software"), to deal
 * in the Software without restriction, subject to the following conditions :
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * The Software is provided "as is", without warranty of any kind.
 *
 * May       2003 - Original version
 * February  2004 - Tidied up.
 * August    2005 - Specification of distfn changed to allow model fitter to
 *                  return multiple models from which the best must be selected
 * Sept      2006 - Random selection of data points changed to ensure duplicate
 *                  points are not selected.
 * February  2007 - Jordi Ferrer : Arranged warning printout.
 *                                Allow maximum trials as optional parameters.
 *                                Patch the problem when non - generated data
 *                                set is not given in the first iteration.
 * August    2008 - 'feedback' parameter restored to argument list and other
 *                  breaks in code introduced in last update fixed.
 * December  2008 - Octave compatibility mods
 * June      2009 - Argument 'MaxTrials' corrected to 'maxTrials'!
 * January   2013 - Separate code path for Octave no longer needed
 * September 2014 - Ported to C/C++ with Armadillo
 * 
 */

#include <c/sampling.h>
#include <c/fitting.h>
#include <c/util.h>

using namespace arma;
MORLIB_NAMESPACE_USE;

#define MB_AS_BYTES(x) ((x) * 1024 * 1024)

MORLIB_API fmat MORLIB_NAMESPACE_PREFIX ransac(const fmat& x, fitting_func fittingfn, distance_func distfn, degenerate_func degenfn, arma::uword s, float t,
	uvec* inliers, bool feedback, unsigned int max_data_trials, unsigned int max_trials, float p)
{
	auto npts = x.n_cols;

	// Sentinel value allowing detection of solution failure.
	auto bestM = fmat();
	auto hasBestM = false;
	auto trialcount = 0u;
	uword bestscore = 0u;
	// Dummy initialisation for number of trials.
	auto N = 1.0f;

	bool degenerate;
	unsigned int count;
	uvec bestinliers;

	while (N > trialcount)
	{
		/*
		 * Select at random s datapoints to form a trial model, M.
		 * In selecting these points we have to check that they are not in
		 * a degenerate configuration.
		 */
		degenerate = true;
		count = 1;
		fmat M_var;
		while (degenerate)
		{
			// Generate s random indicies in the range 0..npts - 1
			const umat ind = random_sample(npts, s);

			// Test that these points are not a degenerate configuration.
			degenerate = degenfn(x.cols(ind));

			if (!degenerate)
			{
				/*
				 * Fit model to this random selection of data points.
				 * Note that M may represent a set of models that fit the data in
				 * this case M will be a cell array of models
				 */
				M_var = fittingfn(x.cols(ind));

				/*
				 * Depending on your problem it might be that the only way you
				 * can determine whether a data set is degenerate or not is to
				 * try to fit a model and see if it succeeds.  If it fails we
				 * reset degenerate to true.
				 */
				degenerate = M_var.is_empty();
			}

			// Safeguard against being stuck in this loop forever
			count++;
			if (count > max_data_trials)
			{
				if (feedback)
				{
					printf("Unable to select a nondegenerate data set");
				}
				break;
			}
		}

		/*
		 * Once we are out here we should have some kind of model...
		 * Evaluate distances between points and model returning the indices
		 * of elements in x that are inliers.Additionally, if M is a cell
		 * array of possible models 'distfn' will return the model that has
		 * the most inliers.After this call M will be a non - cell object
		 * representing only one model.
		 */
		auto inliers_var = distfn(M_var, M_var, x, t);

		// Find the number of inliers to this model.
		auto ninliers = length(inliers_var);

		if (ninliers > bestscore) // Largest set of inliers so far...
		{
			bestscore = ninliers; // Record data for this model
			bestinliers = inliers_var;
			bestM = M_var;
			hasBestM = true;

			/*
			 * Update estimate of N, the number of trials to ensure we pick,
			 * with probability p, a data set with no outliers.
			 */
			float fracinliers = ninliers / static_cast<float>(npts);
			float pNoOutliers = 1.0f - powf(fracinliers, static_cast<float>(s));
			pNoOutliers = std::max(eps(1.0f), pNoOutliers);     // Avoid division by -Inf
			pNoOutliers = std::min(1 - eps(1.0f), pNoOutliers); // Avoid division by 0.
			N = log(1 - p) / log(pNoOutliers);
		}

		trialcount++;
		if (feedback)
		{
			printf("trial %d out of %d         \r", trialcount, ceil(N));
		}

		// Safeguard against being stuck in this loop forever
		if (trialcount > max_trials)
		{
			if (feedback)
			{
				printf("ransac reached the maximum number of %d trials", max_trials);
			}
			break;
		}
	}

	if (feedback)
	{
		printf("\n");
	}

	if (hasBestM) // We got a solution
	{
		if (inliers)
		{
			*inliers = bestinliers;
		}
		return bestM;
	}
	return fmat();
}

MORLIB_API fmat /*MORLIB_NAMESPACE_PREFIX */optimal_ransac(const fmat& x, fitting_func fittingfn, distance_func distfn, uword s, float t,
	float acc, umat* inliers, unsigned int* iter_out, unsigned int max_data_trials, unsigned int max_trials, unsigned int maxit, unsigned int nrtrials, uword low, unsigned int ner)
{
	auto nes = 0u;    // Number of equal sets
	auto maxinl = 0u; // Maximum number of inliers
	auto iter = 0u;   // Number of iterations
	auto t1 = 0.0;    // Time for ordinary RANSAC sampling
	auto t2 = 0.0;    // Time for the extra OPTIMAL resampling / rescoring
	fmat M;

	while (nes < ner && iter < max_trials)
	{
		auto startTime = counterMS();

		//TODO

		t2 += counterMS() - startTime;
	}

	// Compute the total number of iterations in terms of ordinary ransac iterations
	if (iter_out && t1 > 0.0f)
	{
		*iter_out = static_cast<unsigned int>(std::floor((t2 / t1) * iter) + iter);
	}
	return M;
}

MORLIB_API fmat MORLIB_NAMESPACE_PREFIX fast_sampling_plane_filtering_depth(const fmat& i, float fov_h, float fov_v, sword neighborhood_range, float plane_size, uword local_samples, float s, float t,
	fspf_params_t* params)
{
	auto bound_handler = [fov_h, fov_v](const fmat& plane_points, float plane_size, uword width, uword height, uword* bound_width, uword* bound_height)
	{
		auto z = (plane_points.col(0)[2] + plane_points.col(1)[2] + plane_points.col(2)[2]) / 3.0f;
		*bound_width = fast_round_uword(width * (plane_size / z) * tanf(fov_h));
		*bound_height = fast_round_uword(height * (plane_size / z) * tanf(fov_v));
		return true;
	};

	const fmat XYZ = depth_image_to_point_cloud(i, fov_h, fov_v, DEPTH_CONVERT_ALL_ZERO_NEGATIVE_SET_DEPTH);

	return fast_sampling_plane_filtering_points(XYZ, i.n_cols, i.n_rows, neighborhood_range, plane_size, local_samples, s, t,
		bound_handler, params);
}

MORLIB_API fmat MORLIB_NAMESPACE_PREFIX fast_sampling_plane_filtering_points(const fmat& XYZ_src, uword width, uword height, 
	sword neighborhood_range, float plane_size, uword local_samples, float s, float t, plane_bound_calc_func bound_calc, 
	fspf_params_t* paramsPtr)
{
	if (width < 16 || height < 16)
	{
		// Arbitrary depth image size requirement
		return fmat();
	}
	if (XYZ_src.n_rows != (width * height))
	{
		// Specified width/height aren't correct. Maybe they filtered the XYZ before hand?
		return fmat();
	}

	fspf_params_t params = FSPF_DEFAULT_PARAMS;
	if (paramsPtr)
	{
		memcpy(&params, paramsPtr, sizeof(fspf_params_t));
	}

#ifdef _DEBUG
	if (!any(XYZ_src.col(2) > 0))
	{
		if (params.feedback)
		{
			printf("Empty 3D point cloud\n");
		}
		return fmat();
	}
#endif

#define INVALID_DUPLICATES (params.plane_inlier_counts || params.inlier_indices || params.aa_extends || params.obb_stats || params.obb_corners)
	if (params.feedback && INVALID_DUPLICATES && params.remove_duplicates)
	{
		printf("Duplicate removal cannot be used with current params. Disabling.\n");
	}
	params.remove_duplicates = INVALID_DUPLICATES ? false : params.remove_duplicates;
#undef INVALID_DUPLICATES

	// Inliers/outliers/other
	uword P_count = 0;
	fmat P(params.inlier_preallocate, 3);
	fmat R;
	fmat O;
	uvec plane_inliers;
	uvec::fixed<1> plane_inlier_count; // Only here because uvec::insert_rows won't take values (expected, but annoying in that this is needed instead)

	// Performance
	double tmp_performance;
	vec7 performance_results;
#define START_TIMER() if (params.performance) { tmp_performance = counterMS(); }
#define STOP_TIMER(index) if (params.performance) { performance_results.at(index) += counterMS() - tmp_performance; }
	
	// AABB
	frowvec6 aabb;
	fmat bounding_box;

	// OBB
	fmat obb_projection;
	fmat obb_projection_midstep;
	frowvec::fixed<15> obb_stats;
	fmat oriented_bounding_box_stats(0, 15);
	frowvec::fixed<24> obb_corners;
	fmat oriented_bounding_box_corners(0, 24);

	// Plane calculation
#ifndef PLANE_FILTERING_FAST_PLANE_NORMAL_CALCULATION
#ifdef PLANE_FILTERING_4x4_SVD
	fmat44 fitU;
	fvec4 fitS;
	fmat44 fitV;
#else
	fvec3 fitS;
	fmat33 fitV;
#endif
#endif

	fmat33 n;
	uvec real_indicies;
	umat returned_real_indicies;

	frowvec best_plane;
	uword best_inliers = 0;

	// Local, editable, matrix
	fmat XYZ = XYZ_src;

	// Performance timer (if used)
	if (params.performance)
	{
		performance_results.zeros();
		performance_results.at(0) = counterMS();
	}

	auto k = 0u;
	while (P_count < params.max_filtered_points && k++ < params.max_neighborhoods)
	{
		// Pick random points from image (XXX this produces warnings for the arguments of mor_rand_mf and mor_rand_mmf. Casting will fix this, but look really ugly)
		auto d0x = fast_round_uword(rand_mf(width - 1));
		auto d0y = fast_round_uword(rand_mf(height - 1));
		auto d1x = d0x + fast_round_uword(rand_mmf(-neighborhood_range, neighborhood_range));
		auto d1y = d0y + fast_round_uword(rand_mmf(-neighborhood_range, neighborhood_range));
		auto d2x = d0x + fast_round_uword(rand_mmf(-neighborhood_range, neighborhood_range));
		auto d2y = d0y + fast_round_uword(rand_mmf(-neighborhood_range, neighborhood_range));
		d1x = std::min(d1x, width - 1);
		d1y = std::min(d1y, height - 1);
		d2x = std::min(d2x, width - 1);
		d2y = std::min(d2y, height - 1);

#define XYZ_INDEX(x, y) ((y) + (x) * height)

		// Get points for plane
		n.col(0) = XYZ.row(XYZ_INDEX(d0x, d0y)).t();
		n.col(1) = XYZ.row(XYZ_INDEX(d1x, d1y)).t();
		n.col(2) = XYZ.row(XYZ_INDEX(d2x, d2y)).t();
		if (is_colinear(n.col(0), n.col(1), n.col(2)))
		{
			if (params.reset_on_fail)
			{
				k--;
			}
			if (params.feedback)
			{
				printf("Plane is colinear, restarting plane search\n");
			}
			continue;
		}

#undef XYZ_INDEX

		// Generate normal and plane bounds
		const fcolvec r = normalise(cross(n.col(1) - n.col(0), n.col(2) - n.col(0)));
		uword w2;
		uword h2;
		if (!bound_calc(n, plane_size, width, height, &w2, &h2))
		{
			if (params.reset_on_fail)
			{
				k--;
			}
			if (params.feedback)
			{
				printf("Could not calculate bounds for sample\n");
			}
			continue;
		}

		auto subx = d0x - w2;
		auto suby = d0y - h2;
		auto subw = d0x + w2;
		auto subh = d0y + h2;
		if (static_cast<sword>(subx) < 0)
		{
			subx = 0;
		}
		if (subw > width)
		{
			subw = width;
		}
		if (static_cast<sword>(suby) < 0)
		{
			suby = 0;
		}
		if (subh > height)
		{
			subh = height;
		}
		subw -= subx;
		subh -= suby;
		if (!subw || !subh)
		{
			if (params.reset_on_fail)
			{
				k--;
			}
			if (params.feedback)
			{
				printf("Depth image sub-window is zero width and/or zero height, restarting plane search\n");
			}
			continue;
		}

		// plane-specific RANSAC
		const fmat XYZ_sub = point_cloud_sub_window(XYZ, width, height, subx, suby, subw, subh, DEPTH_CONVERT_IGNORE_ZERO_NEGATIVE, &real_indicies);
		if (!XYZ_sub.n_rows)
		{
			if (params.reset_on_fail)
			{
				k--;
			}
			if (params.feedback)
			{
				printf("All points within depth image sub-window are invalid, restarting plane search\n");
			}
			continue;
		}

		// * From planeptdist (fitting.cpp)
		auto d = // distance values
			((XYZ_sub.col(0) - n.at(0, 0)) * r[0]) +
			((XYZ_sub.col(1) - n.at(1, 0)) * r[1]) +
			((XYZ_sub.col(2) - n.at(2, 0)) * r[2]);
		const uvec inliers = find(abs(d) < t);

		// Determine if we want to save the inliers or not
		if (inliers.n_rows > 0)
		{
			if (length(inliers) >= (length(XYZ_sub) * s))
			{
				// Save results
				auto inliers_rows = XYZ_sub.rows(inliers);
				if ((P.n_rows - P_count) < inliers.n_elem)
				{
					P.resize(P_count + inliers.n_elem, 3);
				}
				P.rows(P_count, P_count + inliers.n_elem - 1) = inliers_rows;
#ifdef PLANE_FILTERING_FAST_PLANE_NORMAL_CALCULATION
				R.insert_cols(R.n_cols, r);
#else
#ifdef PLANE_FILTERING_4x4_SVD
				if (!fit_plane_svd(inliers_rows.t(), fitU, fitS, fitV) && params.feedback)
				{
					printf("Failed to fit plane. Result %d is invalid\n", R.n_cols);
				}
				R.insert_cols(R.n_cols, fitV.col(3));
#else
				if (!fit_plane_3x3_pca(inliers_rows.t(), fitS, fitV) && params.feedback)
				{
					printf("Failed to fit plane. Result %d is invalid\n", R.n_cols);
				}
				R.insert_cols(R.n_cols, fitV.col(2));
#endif
#endif
				P_count += inliers.n_elem;

				// Bounding boxes (used for feature extraction)
				if (params.aa_extends)
				{
					START_TIMER();

					aabb.cols(0, 2) = min(inliers_rows);
					aabb.cols(3, 5) = max(inliers_rows);
					bounding_box.insert_rows(bounding_box.n_rows, aabb);

					STOP_TIMER(2);
				}
				if (params.obb_stats || params.obb_corners)
				{
					START_TIMER();

#ifdef PLANE_FILTERING_FAST_PLANE_NORMAL_CALCULATION
#ifdef PLANE_FILTERING_4x4_SVD
					fmat44 fitU;
					fvec4 fitS;
					fmat44 fitV;
					if (!fit_plane_svd(inliers_rows.t(), fitU, fitS, fitV) && params.feedback)
					{
						printf("Failed to generate results for OBB. Result %d is invalid\n", R.n_cols);
					}
#else
					fvec3 fitS;
					fmat33 fitV;
					if (!fit_plane_3x3_pca(inliers_rows.t(), fitS, fitV) && params.feedback)
					{
						printf("Failed to generate results for OBB. Result %d is invalid\n", R.n_cols);
					}
#endif
#endif
					auto xAxis = fitV.submat(span(0, 2), span(0));
					auto yAxis = fitV.submat(span(0, 2), span(1));
					auto zAxis = fitV.submat(span(0, 2), span(2));

					// General
					obb_stats.cols(0, 2) = mean(inliers_rows); // center
					obb_stats.cols(6, 8) = xAxis.t(); // X-axis
					obb_stats.cols(9, 11) = yAxis.t(); // Y-axis
					obb_stats.cols(12, 14) = zAxis.t(); // Z-axis

					// Calculate width, height, and depth
					obb_projection_midstep = inliers_rows - repmat(obb_stats.cols(0, 2), inliers.n_elem, 1);
					obb_projection = obb_projection_midstep * xAxis;
					obb_stats.col(3) = max(obb_projection) - min(obb_projection); // Width
					obb_projection = obb_projection_midstep * yAxis;
					obb_stats.col(4) = max(obb_projection) - min(obb_projection); // Height
					obb_projection = obb_projection_midstep * zAxis;
					obb_stats.col(5) = max(obb_projection) - min(obb_projection); // Depth

					// Save stats
					oriented_bounding_box_stats.insert_rows(oriented_bounding_box_stats.n_rows, obb_stats);

					STOP_TIMER(3);

					// Corner calculation
					if (params.obb_corners)
					{
						START_TIMER();

						// Get half with and transposed axis
						float halfWidth = obb_stats.at(3) * 0.5f;
						float halfHeight = obb_stats.at(4) * 0.5f;
						float halfDepth = obb_stats.at(5) * 0.5f * (params.is_obb_corners_box ? 1 : 0);
						const frowvec3 xAxisTrans = obb_stats.cols(6, 8) * halfWidth;
						const frowvec3 yAxisTrans = obb_stats.cols(9, 11) * halfHeight;
						const frowvec3 zAxisTrans = obb_stats.cols(12, 14) * halfDepth;

						// Generate points
						obb_corners.cols(0, 2) = xAxisTrans + yAxisTrans + zAxisTrans;
						obb_corners.cols(3, 5) = xAxisTrans - yAxisTrans + zAxisTrans;
						obb_corners.cols(6, 8) = -xAxisTrans - yAxisTrans + zAxisTrans;
						obb_corners.cols(9, 11) = -xAxisTrans + yAxisTrans + zAxisTrans;
						if (params.is_obb_corners_box)
						{
							obb_corners.cols(12, 14) = xAxisTrans + yAxisTrans - zAxisTrans;
							obb_corners.cols(15, 17) = xAxisTrans - yAxisTrans - zAxisTrans;
							obb_corners.cols(18, 20) = -xAxisTrans - yAxisTrans - zAxisTrans;
							obb_corners.cols(21, 23) = -xAxisTrans + yAxisTrans - zAxisTrans;
						}

						// Translate all corners to object position
						obb_corners += repmat(obb_stats.cols(0, 2), 1, 8);

						// Save corners
						oriented_bounding_box_corners.insert_rows(oriented_bounding_box_corners.n_rows, obb_corners);

						STOP_TIMER(4);
					}
				}
				if (params.remove_duplicates && P_count * 3 * sizeof(float) > MB_AS_BYTES(16))
				{
					P = unique_rows(P);
					P_count = P.n_rows;
				}
				if (params.feedback)
				{
#ifdef PLANE_FILTERING_FAST_PLANE_NORMAL_CALCULATION
					printf("Found plane %d at iteration %d: x: %f, y: %f, z: %f\n", R.n_cols, k, r(0), r(1), r(2));
#else
#ifdef PLANE_FILTERING_4x4_SVD
					auto norm = fitV.col(3);
#else
					auto norm = fitV.col(2);
#endif
					printf("Found plane %d at iteration %d: x: %f, y: %f, z: %f\n", R.n_cols, k, norm(0), norm(1), norm(2));
#endif
				}

				// Extras...
				if (params.feedback && best_inliers < inliers.n_elem)
				{
					best_inliers = inliers.n_elem;
					best_plane = r.t();
				}
				if (params.plane_inlier_counts)
				{
					plane_inlier_count[0] = inliers.n_elem;
					plane_inliers.insert_rows(plane_inliers.n_elem, plane_inlier_count);
				}
				if (params.inlier_indices)
				{
					START_TIMER();

					auto current_col_count = returned_real_indicies.n_cols;
					if (current_col_count < inliers.n_elem)
					{
						if (current_col_count == 0)
						{
							// Initial setup. set_size is a lot faster then resize
							returned_real_indicies.set_size(1, inliers.n_elem);
						}
						else
						{
							// Extend row length and add a row
							returned_real_indicies.resize(returned_real_indicies.n_rows + 1, std::max(current_col_count, inliers.n_elem));
						}
					}
					else
					{
						// Only add a row
						returned_real_indicies.resize(returned_real_indicies.n_rows + 1, current_col_count);
					}

					// Insert inliers
					returned_real_indicies.submat(span(returned_real_indicies.n_rows - 1), span(0, inliers.n_elem - 1)) = real_indicies(inliers).t();

					STOP_TIMER(5);
				}

				// "Remove" all inliers for the plane (would rather remove the rows themselves, but that would screw up point_cloud_sub_window and may take longer due to memory handling)
				XYZ.rows(real_indicies(inliers)).zeros();
			}
			else if (params.outlier_points) // Saves huge amount of performance time
			{
				START_TIMER();

				O.insert_rows(O.n_rows, XYZ_sub.rows(inliers));

				STOP_TIMER(6);

				if (params.remove_duplicates && O.n_elem * sizeof(float) > MB_AS_BYTES(16))
				{
					O = unique_rows(O);
				}
			}
		}
	}
	if (params.performance)
	{
		performance_results.at(1) = counterMS() - performance_results.at(0);
	}

	if (P_count < P.n_rows)
	{
		P.resize(P_count, 3);
	}
	if (params.feedback)
	{
		printf("Best plane was: (x: %f, y: %f, z: %f) and had %d inliers\n", best_plane(0), best_plane(1), best_plane(2), best_inliers);
		printf("Point count: %d, iterations: %d\n", P.n_rows, (k - 1));
	}
	if (params.plane_normals)
	{
		*params.plane_normals = params.remove_duplicates ? unique_cols(R).t() : R.t();
	}
	if (params.outlier_points)
	{
		*params.outlier_points = params.remove_duplicates ? unique_rows(O) : O;
	}
	if (params.inlier_indices)
	{
		*params.inlier_indices = returned_real_indicies;
	}
	if (params.plane_inlier_counts)
	{
		*params.plane_inlier_counts = plane_inliers;
	}
	if (params.aa_extends)
	{
		*params.aa_extends = bounding_box;
	}
	if (params.obb_stats)
	{
		*params.obb_stats = oriented_bounding_box_stats;
	}
	if (params.obb_corners)
	{
		*params.obb_corners = params.is_obb_corners_box ? oriented_bounding_box_corners : oriented_bounding_box_corners.cols(0, 11);
	}
	if (params.performance)
	{
		performance_results.at(0) = counterMS() - performance_results.at(0);
		*params.performance = performance_results;
	}
	return params.remove_duplicates ? unique_rows(P) : P;
}
