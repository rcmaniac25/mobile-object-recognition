/*
 * Copyright(c) 2014 - 2015 Vincent Simonetti
 * Stevens Institute of Technology
 * vsimonet at stevens edu
 *
 * --------------------------------------------------------------
 *
 * "fit_plane", "ransac_fit_plane"
 *
 * Copyright(c) 2003-2008 Peter Kovesi
 * School of Computer Science & Software Engineering
 * The University of Western Australia
 * http://www.csse.uwa.edu.au/~pk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files(the "Software"), to deal
 * in the Software without restriction, subject to the following conditions :
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * The Software is provided "as is", without warranty of any kind.
 *
 * --------------------------------------------------------------
 *
 * "mor_prune_set"
 *
 * Copyright (c) 2011-2013 Anders Hast
 * Uppsala University
 * http://www.cb.uu.se/~aht
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * The Software is provided "as is", without warranty of any kind.
 *
 * AHT 30/1 2013.  homogdist2d in OPTIMALRANSACFITHOMOGRAPHY was changed to 
 *                 return also the distances. This is not done in the original
 *                 code by Peter Kovesi. 
 * VST 31/10 2014. Ported to C/C++ with Armadillo
 */

#include <c/fitting.h>
#include <c/util.h>
#include <c/sampling.h>

using namespace arma;
MORLIB_NAMESPACE_USE;

// Based off fit_plane_svd
MORLIB_API bool MORLIB_NAMESPACE_PREFIX fit_plane_3x3_pca(const fmat& XYZ, fvec& latent, fmat& coeff)
{
	if (XYZ.n_rows != 3)
	{
		// data is not 3D
		return false;
	}

	auto npts = XYZ.n_cols;

	if (npts < 3)
	{
		// too few points to fit plane
		return false;
	}

	fmat33 A = XYZ * XYZ.t();

	fmat33 score;
	return princomp(coeff, score, latent, A);
}

MORLIB_API fcolvec MORLIB_NAMESPACE_PREFIX fit_plane_3x3(const fmat& XYZ)
{
	fvec3 latent;
	fmat33 coeff;
	if (fit_plane_3x3_pca(XYZ, latent, coeff))
	{
		return coeff.col(2); // Solution is last column of coeff.
	}
	return fcolvec();
}

/*
 * June 2003
 * October 2014 - Ported to C/C++ with Armadillo (with slight modification to return to return SVD results)
 */
MORLIB_API bool MORLIB_NAMESPACE_PREFIX fit_plane_svd(const fmat& XYZ, fmat& U, fvec& S, fmat& V)
{
	if (XYZ.n_rows != 3)
	{
		// data is not 3D
		return false;
	}

	auto npts = XYZ.n_cols;

	if (npts < 3)
	{
		// too few points to fit plane
		return false;
	}

	/*
	 * Set up constraint equations of the form  AB = 0,
	 * where B is a column vector of the plane coefficients
	 * in the form  b[0]*X + b[1]*Y +b[2]*Z + b[3] = 0.
	 */

	fmat A = join_horiz(XYZ.t(), ones<fmat>(npts, 1)); // Build constraint matrix

	if (npts == 3) // Pad A with zeros
	{
		A = join_vert(A, zeros<fmat>(1, 4));
	}

	/* Slight change from original code, multiple the transpose and itself together to geta  4x4 matrix that SVD can run very fast and memory efficent */
	A = A.t() * A;
	/* End change */

	return svd(U, S, V, A); // Singular value decomposition. //XXX Might be faster if done manually, since it will always be a 4x4 matrix
}

MORLIB_API fcolvec MORLIB_NAMESPACE_PREFIX fit_plane(const fmat& XYZ)
{
	fmat44 U;
	fvec4 S;
	fmat44 V;
	if (fit_plane_svd(XYZ, U, S, V))
	{
		return V.col(3); // Solution is last column of v.
	}
	return fcolvec();
}

/*
 * ransacfitplane function and helpers
 *
 * June 2003 - Original version.
 * Feb  2004 - Modified to use separate ransac function
 * Aug  2005 - planeptdist modified to fit new ransac specification
 * Dec  2008 - Much faster distance calculation in planeptdist (thanks to Alastair Harrison) 
 * Oct  2014 - Ported to C/C++ with Armadillo
 */

/*
 * Function to define a plane given 3 data points as required by
 * RANSAC. In our case we use the 3 points directly to define the plane.
 */
fmat defineplane(const fmat& X)
{
	return X;
}

/*
 * Function to calculate distances between a plane and a an array of points.
 * The plane is defined by a 3x3 matrix, P. The three columns of P defining
 * three points that are within the plane.
 */
uvec planeptdist(fmat& P_out, const fmat& P_in, const fmat& X, float t)
{
	fvec n = normalise(cross(P_in.col(1) - P_in.col(0), P_in.col(2) - P_in.col(0))); // Plane normal.

	fmat d = zeros<fmat>(length(X), 1); // d will be an array of distance values.

	/*
	 * The following loop builds up the dot product between a vector from P.col(0)
	 * to every X.col(i) with the unit plane normal.  This will be the
	 * perpendicular distance from the plane for each point
	 */
	for (unsigned int i = 0; i < 3; i++)
	{
		d += (X.row(i).t() - P_in.at(i, 0)) * n[i];
	}
	P_out = P_in;

	return find(abs(d) < t);
}

/*
 * Function to determine whether a set of 3 points are in a degenerate
 * configuration for fitting a plane as required by RANSAC. In this case
 * they are degenerate if they are colinear.
 */
bool isdegenerate(const fmat& X)
{
	// The three columns of X are the coords of the 3 points.
	return is_colinear(X.col(0), X.col(1), X.col(2));
}

MORLIB_API fcolvec MORLIB_NAMESPACE_PREFIX ransac_fit_plane(const fmat& XYZ, float t, fmat* P, uvec* inliers, bool feedback)
{
	if (XYZ.n_rows != 3)
	{
		// data is not 3D
		return fcolvec();
	}

	if (XYZ.n_cols < 3)
	{
		// too few points to fit plane
		return fcolvec();
	}

	auto s = 3u; // Minimum No of points needed to fit a plane.

	uvec inliers_ref;
	fmat P_result = ransac(XYZ, defineplane, planeptdist, isdegenerate, s, t, &inliers_ref, feedback);

	if (P)
	{
		*P = P_result;
	}
	if (inliers)
	{
		*inliers = inliers_ref;
	}

	// Perform least squares fit to the inlying points
	return fit_plane(XYZ.cols(inliers_ref));
#if 0
	auto sub = XYZ.cols(inliers_ref);
	/*
	fmat sub;
	sub << 2 << 0 << 2 << endr
		<< 1 << 2 << 0 << endr
		<< 0 << 0 << 1 << endr
		<< 6 << 5 << 1 << endr;
	sub = sub.t();
	*/

	fmat44 U;
	fvec4 S;
	fmat44 V;
	fmat33 U2;
	fvec3 S2;
	fmat33 V2;
	bool suc = fit_plane_svd(sub, U, S, V);
	bool suc2 = fit_plane_3x3_svd(sub, U2, S2, V2);

	if (suc)
	{
		return V.col(3);
	}
	return fcolvec();
#endif
}

MORLIB_API fmat /*MORLIB_NAMESPACE_PREFIX */prune_set(const fmat& H, const fmat& x, float acc, fitting_func fittingfn, distance_func distfn, umat* putative, uword* len)
{
	auto len_ref = x.n_cols;
	auto H_ref = H;

	auto repeat = true;
	umat putative_ref;
	putative_ref.reshape(1, len_ref);
	fill_index(putative_ref);
	while (len_ref > 5 && repeat)
	{
		// Compute the distances
		fmat d2;
		auto inliers = distfn(d2, H, x.cols(putative_ref), acc);

		// Remove the most extreme point
		const fmat t1 = max(d2);
		if (t1(0) > acc)
		{
			//XXX must find all elements of d2 that "max"
			//shed_cols(putative_ref, ?);
			H_ref = fittingfn(x.cols(putative_ref));
		}
		else
		{
			repeat = false;
		}

		len_ref = putative_ref.n_cols;
	}
	if (len)
	{
		*len = len_ref;
	}
	if (putative)
	{
		*putative = putative_ref;
	}
	return H_ref;
}
