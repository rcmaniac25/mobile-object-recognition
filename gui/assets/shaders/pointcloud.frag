﻿#version 420

in vec3 Color;

out vec4 fColor;

void main() 
{ 
	fColor = vec4(Color, 1.0);
    //gl_FragColor = vec4(0, .5, 0, 1.0);
} 