﻿#version 420

//uniform   vec4  color;
in vec2 texCoord;

layout(binding=0) uniform sampler2D tex;

void main() 
{ 
	gl_FragColor = texture(tex, texCoord);
    //gl_FragColor = vec4(0, .5, 0, 1.0);
} 