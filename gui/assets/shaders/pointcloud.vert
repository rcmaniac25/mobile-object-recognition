﻿#version 420

in vec3 vPosition;
in vec3 vColor;

out vec3 Color;

uniform mat4 Projection;

void main()
{
	Color = vColor;
	
	vec3 position = vPosition;
	position.z = -position.z;
	position.y = -position.y;
	position.x = -position.x;
	gl_Position = Projection * vec4(position, 1.0);
}