#pragma once

#include "./KinectManager.h"
#include "./Config.h"

#include <armadillo>
#include <mor.hpp>

#include <vector>
#include <string>

class MatFileManager : public mor::ISensor
{
public:
	enum FilterType
	{
		FILTER_LABEL,
		FILTER_OUT_LABEL,

		FILTER_DISABLED
	};

	MatFileManager(const Config& config);
	~MatFileManager();

	inline float getFovY() const { return float((NUI_CAMERA_DEPTH_NOMINAL_VERTICAL_FOV * M_PI) / 180.0f); }
	inline float getFovX() const { return float((NUI_CAMERA_DEPTH_NOMINAL_HORIZONTAL_FOV * M_PI) / 180.0f); }

	inline float getFocalLength() const { return NUI_CAMERA_DEPTH_NOMINAL_FOCAL_LENGTH_IN_PIXELS; }
	inline float getInverseFocalLength() const { return NUI_CAMERA_DEPTH_NOMINAL_INVERSE_FOCAL_LENGTH_IN_PIXELS; }
	int getElevationAngle() const;
	bool getAccelerometerReading(float* values) const;

	inline unsigned int getDepthImageWidth() const { return 640; }
	inline unsigned int getDepthImageHeight() const { return 480; }
	inline unsigned int getDepthImageBPP() const { return 1; }

	bool init();
	bool startStreaming();
	void stopStreaming();

	bool isConnected() const;

	bool getDepthContents(arma::fmat* depth, unsigned char* image);

	// Labeling functions (any non-string label is 1-based, as 0 == unlabeled/invalid)
	std::vector<std::string> getCurrentLabels() const;
	inline const std::vector<std::string>& getAllLabels() const { return _nameData; }
	bool getLabeledContents(arma::Mat<arma::u16>* labels) const;
	arma::u16 findLabelIndex(const std::string& label) const;
	const std::string& getLabel(arma::u16 index) const;

	bool filterDepthByLabels(const std::vector<std::string>& labels, FilterType filterType);
	bool clearFilterDepth()
	{
		return filterDepthByLabels(std::vector<std::string>(), FILTER_DISABLED);
	}

	// Extra functions
	inline arma::uword getCurrentIndex() const { return _index; }
	inline void setCurrentIndex(arma::uword index) { _index = index % _indexCount; }
	inline arma::uword getTotalImageCount() const { return _indexCount; }

	inline bool supportsElevationAccelerometer() const { return _accData.n_elem != 0; }
	inline bool supportsLabeling() const { return !_nameData.empty() && !_labels.empty(); }

	inline bool getIncrementImageOnRetrieval() const { return _incIndex; }
	inline void setIncrementImageOnRetrieval(bool enable) { _incIndex = enable; }

private:
	Matfile* _file;

	std::string _depth;

	std::string _acc;
	arma::fmat _accData;
	arma::frowvec4 _accCurrent;

	std::string _names;
	std::vector<std::string> _nameData;
	std::string _labels;
	arma::Mat<arma::u16> _labelDataCurrent;

	FilterType _filterDepth;
	std::vector<u16> _activeFilters;

	arma::uword _index;
	arma::uword _indexCount;
	bool _incIndex;
};
