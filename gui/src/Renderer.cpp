#include "./Renderer.h"
#include "./KinectManager.h"

Renderer::~Renderer()
{
    glfwTerminate();
}

bool Renderer::init(int width, int height)
{
    // initialize GLFW library
    if (!glfwInit())
    {
        return false;
    }

    // Create a new window
    _window = glfwCreateWindow(width, height, "MOR GUI", nullptr, nullptr);
    if (_window == nullptr)
    {
        glfwTerminate();
        return false;
    }

    // Set window to the current opengl context
    glfwMakeContextCurrent(_window);

    GLenum err = glewInit();
    if (err != GLEW_OK)
    {
        glfwTerminate();
        printf("ERROR GLEW INITIALIZATION: %s\n", glewGetErrorString(err));
        return false;
    }

    _camera = std::make_shared<Camera>();
    _camera.get()->setStartZoom(0);

    if (initRenderables() == false)
    {
        glfwTerminate();
        printf("ERROR RENDERABLE INIT\n");
        return false;
    }

    // set clear color to white
    glClearColor(1, 1, 1, 1);
    _mode = Depth;
    return true;
}

bool Renderer::initRenderables()
{
    if (_testRenderable.init() == false)
    {
        return false;
    }

    if (_depthRenderable.init() == false)
    {
        return false;
    }

    if (_pointCloudRenderable.init(_camera.get(), KinectManager::DEPTH_IMAGE_SIZE, GL_POINTS) == false)
    {
        return false;
    }

    if (_planeRenderable.init(_camera.get(), KinectManager::DEPTH_IMAGE_SIZE, GL_POINTS) == false)
    {
        return false;
    }

    if (_quadRenderable.init(_camera.get(), KinectManager::DEPTH_IMAGE_SIZE / 4, GL_TRIANGLES) == false)
    {
        return false;
    }

    return true;
}

bool Renderer::windowIsOpen() const
{
    return glfwWindowShouldClose(_window) == false;
}

void Renderer::requestWindowClose()
{
	glfwSetWindowShouldClose(_window, true);
}

int Renderer::getWidth() const
{
	int width;
	glfwGetWindowSize(_window, &width, nullptr);
	return width;
}

int Renderer::getHeight() const
{
	int height;
	glfwGetWindowSize(_window, nullptr, &height);
	return height;
}

void Renderer::setMode(Mode mode)
{
    _mode = mode;
}

void Renderer::setDepthMap(const GLubyte* data, int width, int height)
{
    _depthRenderable.setTexture(data, width, height);
}

void Renderer::setPointCloudData(const GLfloat* data, const GLfloat* colors, int count)
{
    _pointCloudRenderable.setPointCloudData(data, colors, count);
}

void Renderer::setPlaneData(const GLfloat* data, const GLfloat* colors, int count)
{
    _planeRenderable.setPointCloudData(data, colors, count);
}

void Renderer::setQuadData(const GLfloat* data, const GLfloat* colors, int count)
{
    _quadRenderable.setPointCloudData(data, colors, count);
}

void Renderer::renderFrame()
{
    // clear buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    switch (_mode)
    {
    case Test:
        _testRenderable.render();
        break;
    case Depth:
        _depthRenderable.render();
        break;
    case PointCloud:
        _pointCloudRenderable.render();
        break;
    case Planes:
        _planeRenderable.render();
        break;
    case Quads:
        glEnable(GL_DEPTH_TEST);
        _quadRenderable.render();
        glDisable(GL_DEPTH_TEST);
        break;
    }

    glfwSwapBuffers(_window);
}