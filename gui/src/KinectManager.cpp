#include "./KinectManager.h"

#ifdef USE_KINECT

#ifdef MOR_KINECT_USE_NEAR_MODE
static const USHORT DEPTH_MIN = NuiDepthPixelToDepth(NUI_IMAGE_DEPTH_MINIMUM_NEAR_MODE);
static const USHORT DEPTH_MAX = NuiDepthPixelToDepth(NUI_IMAGE_DEPTH_MAXIMUM_NEAR_MODE);
#else
static const USHORT DEPTH_MIN = NuiDepthPixelToDepth(NUI_IMAGE_DEPTH_MINIMUM);
static const USHORT DEPTH_MAX = NuiDepthPixelToDepth(NUI_IMAGE_DEPTH_MAXIMUM);
#endif

KinectManager::KinectManager() :
	_sensor(nullptr), _depthStream(INVALID_HANDLE_VALUE)
{
}

KinectManager::~KinectManager()
{
	_depthStream = INVALID_HANDLE_VALUE;
	if (_sensor)
	{
		_sensor->NuiShutdown();
		_sensor->Release();
		_sensor = nullptr;
	}
}

inline int KinectManager::getElevationAngle() const
{
	LONG degrees = 0;
	if (!_sensor || FAILED(_sensor->NuiCameraElevationGetAngle(&degrees)))
	{
		return 0;
	}
	return static_cast<int>(degrees);
}

inline bool KinectManager::getAccelerometerReading(float* values) const
{
	Vector4 acc;
	if (!_sensor || FAILED(_sensor->NuiAccelerometerGetCurrentReading(&acc)))
	{
		return false;
	}
	if (values)
	{
		values[0] = acc.x;
		values[1] = acc.y;
		values[2] = acc.z;
		values[3] = acc.w;
	}
	return true;
}

bool KinectManager::init()
{
	if (_sensor)
	{
		return true;
	}

	// Make sure that there is a valid kinect sensor connected
	int sensorCount;
	if (FAILED(NuiGetSensorCount(&sensorCount)) || sensorCount < 1)
	{
		return false;
	}

	for (int i = 0; i < sensorCount; i++)
	{
		// Get a reference to the connected kinect
		if (FAILED(NuiCreateSensorByIndex(i, &_sensor)))
		{
			continue;
		}

		if (_sensor->NuiStatus() == S_OK)
		{
			break;
		}

		_sensor->Release();
		_sensor = nullptr;
	}

	if (!_sensor)
	{
		return false;
	}

	// Initialize the kinect sensor
	if (FAILED(_sensor->NuiInitialize(NUI_INITIALIZE_FLAG_USES_DEPTH)))
	{
		_sensor->Release();
		_sensor = nullptr;
		return false;
	}

	return true;
}

bool KinectManager::startStreaming()
{
	if (!_sensor)
	{
		return false;
	}
	if (_depthStream != INVALID_HANDLE_VALUE)
	{
		return true;
	}

	HRESULT hr = E_FAIL;

	hr = _sensor->NuiImageStreamOpen(
		NUI_IMAGE_TYPE_DEPTH,
		NUI_IMAGE_RESOLUTION_640x480,
		0,
		2, // Frame buffer size
		nullptr,
		&_depthStream);
	if (FAILED(hr))
	{
		return false;
	}

#ifdef MOR_KINECT_USE_NEAR_MODE
	hr = _sensor->NuiImageStreamSetImageFrameFlags(_depthStream, NUI_IMAGE_STREAM_FLAG_ENABLE_NEAR_MODE);
	if (FAILED(hr))
	{
		return false;
	}
#endif

	_sensor->NuiSkeletonTrackingDisable();

	return true;
}

void KinectManager::stopStreaming()
{
	_depthStream = INVALID_HANDLE_VALUE;
	if (_sensor)
	{
		_sensor->NuiShutdown();
	}
}

bool KinectManager::isConnected() const
{
	if (_sensor)
	{
		//XXX do we want any other reasons to consider it "disconnected"?
		if (_sensor->NuiStatus() == E_NUI_NOTCONNECTED)
		{
			return false;
		}
		return true;
	}
	return false;
}

bool KinectManager::getDepthContents(arma::fmat* depthMatrix, unsigned char* image)
{
	if (_depthStream == INVALID_HANDLE_VALUE)
	{
		return false;
	}

	NUI_IMAGE_FRAME depthFrame;
	NUI_LOCKED_RECT lockRect;

	if (FAILED(_sensor->NuiImageStreamGetNextFrame(_depthStream, 0, &depthFrame)))
	{
		return false;
	}
	INuiFrameTexture* texture = depthFrame.pFrameTexture;
	texture->LockRect(0, &lockRect, nullptr, 0);

	unsigned int width = getDepthImageWidth();
	unsigned int imageSize = width * getDepthImageHeight();

	// if the lockRect was initialized
	if (lockRect.Pitch != 0)
	{
		const USHORT* source = (const USHORT*)lockRect.pBits;

		for (unsigned int index = 0; index < imageSize; index++)
		{
			USHORT depth = NuiDepthPixelToDepth(source[index]);

			if (depthMatrix != nullptr)
			{
				// All our current development is based off testing the NYU dataset. They do the following calculation on raw depth values to produce it's depth matrix. (From NYU Toolkit)
				//depthMatrix->at(index % width, index / width) = NYU_DEPTH_NUMERATOR / (NYU_DEPTH_DENOMINATOR - depth);

                depthMatrix->at(index % width, index / width) = depth; 
			}

			if (image != nullptr)
			{
				if (depth == NUI_IMAGE_DEPTH_NO_VALUE) //XXX should "invalid/too-far" be here too?
				{
					image[index] = 0;
				}
				else
				{
					image[index] = static_cast<unsigned char>(256 * (1.0f - ((depth - DEPTH_MIN) / float(DEPTH_MAX - DEPTH_MIN))));
				}
			}
		}
	}

	texture->UnlockRect(0);
	_sensor->NuiImageStreamReleaseFrame(_depthStream, &depthFrame);

	return true;
}
#else
static const USHORT DEPTH_MIN = 800;
static const USHORT DEPTH_MAX = 4000;
#endif