#pragma once

#include "./Renderer.h"
#include "./KinectManager.h"
#include "./Config.h"

class Coordinator
{
public:
	Coordinator(Renderer* renderer, std::shared_ptr<mor::ISensor>& sensor, Config& config);
    ~Coordinator();

    void mainLoop();

private:
	std::shared_ptr<mor::ISensor> _sensor;
    std::string _saveLoadPath;
	std::string _featureFile;
	std::string _filterFeature;
	bool _skipEmptyFrames;
    Renderer* _renderer;
    bool _streaming;
    bool _leftMouseDown;
    bool _rightMouseDown;
    bool _shiftDown;
    double _startX;
    double _startY;

    PointCloud* _cloud;
	PlaneFilteringParams _planeFilteringParams;
    arma::fmat* _depthMatrix;
    BYTE* _depthBuffer;
    std::vector<Point> _renderPoints;
    std::vector<Point> _renderColors;
    std::vector<Point> _renderQuads;
    std::vector<Point> _renderQuadColors;
    std::vector<Point> _colors;

    void getData(bool fillBuffer, bool computePointCloud, bool computePlanes);
	bool internalGetData(bool fillBuffer, bool computePointCloud, bool computePlanes);
    void computeFromDepthMatrix();
    void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
    void mouseCallback(GLFWwindow* window, double xpos, double ypos);
    void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods);

    static Coordinator* _instance;
    static void staticMouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
    static void staticMouseCallback(GLFWwindow* window, double xpos, double ypos);
    static void staticKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
};