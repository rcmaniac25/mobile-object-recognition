#include "./Coordinator.h"
#include <mor.hpp>
#include "MatFileManager.h"

Coordinator* Coordinator::_instance;

const int MaxCount = KinectManager::DEPTH_IMAGE_SIZE;
#define DEFAULT_MAX_NEIGHBORHOOD 20000
#define DEFAULT_INLIER_PREALLOCATION 300000
#define FORCE_STOP_GET_DATA_RECURSION 1000000
#define MIN_INLIERS_MATCH_FEATURE_FOR_MAJOR 0.5f

Coordinator::Coordinator(Renderer* renderer, std::shared_ptr<mor::ISensor>& sensor, Config& config)
    : _renderer(renderer)
    , _sensor(sensor)
{
    assert(_instance == nullptr);
    _instance = this;
    _streaming = config.getInt("streamByDefault");
    _saveLoadPath = config.getString("saveLoadPath");
	_featureFile = config.getString("featurePath");
	_filterFeature = config.getString("filter");
	_skipEmptyFrames = config.getInt("skipEmptyFrame");

	MatFileManager* mfm = dynamic_cast<MatFileManager*>(_sensor.get());

	if (_featureFile.size())
	{
		{
			// Remove file if it exists
			FILE* file = nullptr;
#if defined(_WIN32)
			fopen_s(&file, _featureFile.c_str(), "r");
#else
			file = fopen(_featureFile.c_str(), "r");
#endif
			if (file)
			{
				fclose(file);
				if (remove(_featureFile.c_str()))
				{
					// Couldn't remove file, don't extract features
					mfm = nullptr;
				}
			}
#if defined(_WIN32)
			fopen_s(&file, _featureFile.c_str(), "w");
#else
			file = fopen(_featureFile.c_str(), "w");
#endif
			fprintf(file, "frame,plane index,plane type,inlier count,feature label,majority feature,normalX,normalY,normalZ,centerX,centerY,centerZ,width,height,elevation angle,ground height\n");
			fflush(file);
			fclose(file);
		}

		// If proper sensor is used (MatFileManager), then stream data
		if (mfm && mfm->getIncrementImageOnRetrieval())
		{
			printf("Generating feature matrix\n");
			_streaming = true;
		}
		else
		{
			_featureFile = "";
		}
	}

	// Plane filtering params
	_planeFilteringParams.maxFilteredPoints = MaxCount;
	_planeFilteringParams.maxNeighborhoods = DEFAULT_MAX_NEIGHBORHOOD;
	_planeFilteringParams.inlierPreallocate = DEFAULT_INLIER_PREALLOCATION;
	_planeFilteringParams.rawPlaneNormals = new fmat();
	_planeFilteringParams.rawInlierCount = new uvec();
#ifdef TEST_PERFORMANCE
	_planeFilteringParams.performanceResults = new arma::vec();
#endif
	if (_filterFeature.size())
	{
		if (mfm)
		{
			if (_featureFile.size())
			{
				if (mfm->findLabelIndex(_filterFeature))
				{
					_planeFilteringParams.rawInlierIndicies = new umat();
					printf("using filter: %s.\n", _filterFeature.c_str());
				}
				else
				{
					printf("Could not find filter %s. Not filtering.\n", _filterFeature.c_str());
					_filterFeature = "";
				}
			}
			else
			{
				std::vector<std::string> filters = { _filterFeature };
				if (!mfm->filterDepthByLabels(filters, config.getInt("filterOut") ? MatFileManager::FILTER_OUT_LABEL : MatFileManager::FILTER_LABEL))
				{
					printf("Failed to apply label %s.\n", _filterFeature.c_str());
					_filterFeature = "";
				}
				else
				{
					printf("Applied filter: %s.\n", _filterFeature.c_str());
				}
			}
		}
		else
		{
			printf("Filters can only be used with file based sensors.\n");
		}
	}

    //setup input handling
    glfwSetCursorPosCallback(_renderer->window(), staticMouseCallback);
    glfwSetMouseButtonCallback(_renderer->window(), staticMouseButtonCallback);
    glfwSetKeyCallback(_renderer->window(), staticKeyCallback);
    glfwSetInputMode(_renderer->window(), GLFW_STICKY_KEYS, 1);

    _renderPoints = std::vector<Point>(MaxCount);
    _renderColors = std::vector<Point>(MaxCount);
    _renderQuads = std::vector<Point>(MaxCount / 4);

#define rgb(x, y, z) Point((x)/float(255), (y)/float(255), (z)/float(255)) 
    _colors = std::vector<Point>();
    _colors.push_back(rgb(26, 188, 156));
    _colors.push_back(rgb(26, 188, 156));
    _colors.push_back(rgb(230, 126, 34));
    _colors.push_back(rgb(39, 174, 96));
    _colors.push_back(rgb(41, 128, 185));
    _colors.push_back(rgb(142, 68, 173));
    _colors.push_back(rgb(44, 62, 80));
    _colors.push_back(rgb(127, 140, 141));
    _colors.push_back(rgb(192, 57, 43));
	//colors.push_back(rgb(211, 84k, 0));
	// colors from : http://flatuicolors.com/
#undef rgb

    _depthBuffer = new BYTE[KinectManager::DEPTH_IMAGE_SIZE];
    _depthMatrix = new arma::fmat(KinectManager::DEPTH_HEIGHT, KinectManager::DEPTH_WIDTH);
    _cloud = new PointCloud(*_depthMatrix, _sensor, false);
}

Coordinator::~Coordinator()
{
    _instance = nullptr;
#ifdef TEST_PERFORMANCE
	delete _planeFilteringParams.performanceResults;
#endif
	delete _planeFilteringParams.rawInlierIndicies;
	delete _planeFilteringParams.rawInlierCount;
	delete _planeFilteringParams.rawPlaneNormals;
    delete _depthMatrix;
    delete _cloud;
    delete _depthBuffer;
}

void Coordinator::mainLoop()
{
    _renderer->camera()->setPerspective(_sensor->getFovY(), _renderer->getWidth() / float(_renderer->getHeight()), 0.004f, 4000.0f);
    _renderer->camera()->setCenter(0, 0, 2000);
    while (_renderer->windowIsOpen())
    {
		if (_featureFile.size())
		{
			// Get data, but also close program after getting the last element
			MatFileManager* mfm = dynamic_cast<MatFileManager*>(_sensor.get());
			bool closeWindow = mfm->getCurrentIndex() == (mfm->getTotalImageCount() - 1);
			getData(false, false, true);
			if (closeWindow)
			{
				_renderer->requestWindowClose();
			}
		}
        else if (_streaming)
        {
            bool pointCloud = _renderer->mode() == Renderer::Mode::PointCloud;
            bool texture = _renderer->mode() == Renderer::Mode::Depth;
            bool computePlanes = _renderer->mode() == Renderer::Mode::Planes || _renderer->mode() == Renderer::Mode::Quads;
            getData(texture, pointCloud, computePlanes);
        }

		if (!_sensor->isConnected())
		{
			printf("Sensor was disconnected. Shutting down.\n");
			_renderer->requestWindowClose();
		}

        _renderer->renderFrame();
        glfwPollEvents();
    }
}

void Coordinator::getData(bool fillBuffer, bool computePointCloud, bool computePlanes)
{
	uword stopCount = FORCE_STOP_GET_DATA_RECURSION;
	uword i;
	bool presentedSlowWarning = false;

	MatFileManager* mfm = dynamic_cast<MatFileManager*>(_sensor.get());
	if (mfm)
	{
		// We have an upper bound!
		stopCount = mfm->getTotalImageCount();
	}
	if (_featureFile.size())
	{
		// For feature generation, only run one iteration.
		stopCount = 1;
	}

	for (i = 0; i < stopCount; i++)
	{
		if (internalGetData(fillBuffer, computePointCloud, computePlanes) || !_renderer->windowIsOpen())
		{
			break;
		}
		if (i > 240 && !presentedSlowWarning) // 240 = 10% of 2400 frames (a good amount of frames)
		{
#ifndef DEMO
			printf("getData is still trying to get data. Are you sure your sensor is setup or positioned correctly.\n");
#endif
			presentedSlowWarning = true;
		}
	}
	if (i >= stopCount && stopCount > 1)
	{
		printf("getData has failed to get any meaningful data. If skipEmptyFrame is enabled, you may want to disable it or double check your sensor actually has non empty frames of data.\n");
	}
}

#define DEFAULT_NEIGHBORHOOD_RANGE 30
#define DEFAULT_PLANE_SIZE 240
#define DEFAULT_INLIER_PRECENTAGE 0.21f
#ifdef DEMO
// This is a big factor in speed, but won't give good results. Don't use this for feature generation
#define DEFAULT_INLIER_DISTANCE 90
#else
#define DEFAULT_INLIER_DISTANCE 14
#endif

#ifdef PLANE_FILTERING_INTERACTIVE
static arma::sword neh_range = DEFAULT_NEIGHBORHOOD_RANGE;
static float plane_size = DEFAULT_PLANE_SIZE;
static float inlier_prec = DEFAULT_INLIER_PRECENTAGE;
static float inlier_dist = DEFAULT_INLIER_DISTANCE;
#endif

bool Coordinator::internalGetData(bool fillBuffer, bool computePointCloud, bool computePlanes)
{
	arma::fvec3 gravity(fill::zeros);
	arma::fvec3 groundCentroid(fill::zeros);
    bool matrixFilled = computePointCloud || computePlanes;
	if (_streaming)
	{
		if (!_sensor->getDepthContents(matrixFilled ? _depthMatrix : nullptr,
									   fillBuffer ? _depthBuffer : nullptr))
		{
			return false;
		}
		if (computePlanes)
		{
			if (_sensor->getAccelerometerReading(gravity.memptr()))
			{
				gravity = normalise(gravity);
			}
			else
			{
				gravity.zeros();
				gravity.at(1) = 1; // Standard up vector
			}
		}
    }
	if (_skipEmptyFrames && matrixFilled)
	{
		if (!_featureFile.empty())
		{
			if (_filterFeature.size())
			{
				MatFileManager* mfm = dynamic_cast<MatFileManager*>(_sensor.get());
				u16 labelToFind = mfm->findLabelIndex(_filterFeature);

				Mat<u16> labels;
				if (mfm->getLabeledContents(&labels) && !any(vectorise(labels) == labelToFind))
				{
					return false;
				}
			}
		}
		else
		{
			// Only check depth matrix if we want to skip empty frames
			if (all(vectorise(*_depthMatrix) <= 0.0f))
			{
				return false;
			}
		}
	}

    if (fillBuffer)
    {
        _renderer->setDepthMap(_depthBuffer, KinectManager::DEPTH_WIDTH, KinectManager::DEPTH_HEIGHT);
    }

    if (matrixFilled)
    {
        _cloud->recomputePointCloud(*_depthMatrix);

        if (computePointCloud)
        {
            //Point defaultColor(0, .5, 0);
            //colors.resize(MaxCount);
            for (int i = 0; i < MaxCount; i++)
            {
                _renderColors[i] = _colors[i % _colors.size()];;
            }
            _renderer->setPointCloudData(reinterpret_cast<const GLfloat*>(_cloud->getPoints().data()),
                                         reinterpret_cast<const GLfloat*>(_renderColors.data()), MaxCount);
            //_cloud->writeToFile("test.ply");
        }

        if (computePlanes)
		{
			// Local performance variable
			double localFeatureTotal = 0;

			// Feature creation variables
			u16 featureLabel = 0;
			uword frame = 0;
			FILE* featureFilePtr = nullptr;
			if (_featureFile.size())
			{
				MatFileManager* mfm = dynamic_cast<MatFileManager*>(_sensor.get());
				frame = mfm->getCurrentIndex();
				if (frame == 0)
				{
					// Index looped
					frame = mfm->getTotalImageCount();
				}
				frame -= 1;
				printf("Generating features for frame %d\n", frame);

#if defined(_WIN32)
				fopen_s(&featureFilePtr, _featureFile.c_str(), "a");
#else
				featureFilePtr = fopen(_featureFile.c_str(), "a");
#endif
			}

#ifdef PLANE_FILTERING_INTERACTIVE
			printf("Plane filtering inputs: planeFiltering(%d, %ff, %ff, %ff)\n", neh_range, plane_size, inlier_prec, inlier_dist);
			std::vector<PlaneFilteringResult> results = _cloud->planeFiltering(neh_range, plane_size, inlier_prec, inlier_dist, 
				_planeFilteringParams);
#else
			std::vector<PlaneFilteringResult> results = _cloud->planeFiltering(DEFAULT_NEIGHBORHOOD_RANGE, DEFAULT_PLANE_SIZE, DEFAULT_INLIER_PRECENTAGE, DEFAULT_INLIER_DISTANCE, _planeFilteringParams);
#endif
            printf("results size: %d\n", results.size());

			arma::ivec featurePlanes(_planeFilteringParams.rawPlaneNormals->n_rows, fill::zeros);
			arma::ivec planeCatagories;
			if (_planeFilteringParams.rawPlaneNormals->n_rows)
			{
				// Calculate plane catagories, then find the ground plane
				planeCatagories = mor::categorizePlanes(_planeFilteringParams.rawPlaneNormals->cols(0, 2), *_planeFilteringParams.rawInlierCount, gravity);
				for (uword i = 0; i < planeCatagories.n_elem; i++)
				{
					if ((planeCatagories.at(i) & GROUND) == GROUND)
					{
						groundCentroid = fvec3(&results[i].center.x);
						break;
					}
				}

				// Find feature planes, if returning features
				if (featureFilePtr && _filterFeature.size())
				{
					MatFileManager* mfm = dynamic_cast<MatFileManager*>(_sensor.get());
					Mat<u16> labels;
					if (mfm->getLabeledContents(&labels))
					{
						featureLabel = mfm->findLabelIndex(_filterFeature);
						for (uword i = 0; i < featurePlanes.n_elem; i++)
						{
							auto inlierCount = _planeFilteringParams.rawInlierCount->at(i);
							auto inliers = _planeFilteringParams.rawInlierIndicies->submat(span(i), span(0, inlierCount - 1));
							auto labeledInliers = labels(inliers);
							uvec featuredLabels = find(labeledInliers == featureLabel);
							if (featuredLabels.n_elem >= (inlierCount * MIN_INLIERS_MATCH_FEATURE_FOR_MAJOR))
							{
								featurePlanes.at(i) = true;
							}
						}
					}
				}
			}

            // for each result
            //      copy point data into buffer
            //      assign color by colors[index % color.size]
            //_renderer->setPlanes(results);

            _renderQuads.resize(results.size() * 6);
            _renderQuadColors.resize(results.size() * 6);

            int currentIndex = 0;
            int quadIndex = 0;
            for (int resultsi = 0; resultsi < results.size(); resultsi++)
            {
				// Render points
                std::vector<Point>& points = results[resultsi].inliers;
                for (int pointsi = 0; pointsi < points.size(); pointsi++)
                {
                    _renderPoints[currentIndex] = points[pointsi];
                    _renderColors[currentIndex] = _colors[resultsi % _colors.size()];
                    currentIndex++;
                }

				// Planes
#ifndef TEST_PLANE
                Point* quad = results[resultsi].planeCorners;
#else
                float dim = 400;
                float offset = 4000 - (100 * resultsi);
                Point* quad = new Point[4];
                quad[0] = Point(-dim, -dim, offset);
                quad[1] = Point(-dim, dim, offset);
                quad[2] = Point(dim, dim, offset);
                quad[3] = Point(dim, -dim, offset);
#endif

                _renderQuads[quadIndex] = quad[0];
                _renderQuadColors[quadIndex++] = _colors[resultsi % _colors.size()];
                _renderQuads[quadIndex] = quad[3];
                _renderQuadColors[quadIndex++] = _colors[resultsi % _colors.size()];
                _renderQuads[quadIndex] = quad[1];
                _renderQuadColors[quadIndex++] = _colors[resultsi % _colors.size()];
                _renderQuads[quadIndex] = quad[1];
                _renderQuadColors[quadIndex++] = _colors[resultsi % _colors.size()];
                _renderQuads[quadIndex] = quad[3];
                _renderQuadColors[quadIndex++] = _colors[resultsi % _colors.size()];
                _renderQuads[quadIndex] = quad[2];
                _renderQuadColors[quadIndex++] = _colors[resultsi % _colors.size()];

#ifdef TEST_PLANE
				delete quad;
#endif

				// Plane information
				{
					const PlaneFilteringResult& planeResult = results[resultsi];

					Timer timer(true);
					fvec3 planeNormal = normalise(fvec3(&planeResult.plane.a)); // the normal corresponding to that plane //XXX is the plane not already normal?
					float planeElavationAngle = M_PI_2 - acosf(dot(planeNormal, -gravity));
					const Point& planeCenter = planeResult.center;
					float planeGroundHeight = planeCenter.y - groundCentroid.at(1);
					PlaneType planeType = static_cast<PlaneType>(planeCatagories.at(resultsi));
					uword planeInlierCount = _planeFilteringParams.rawInlierCount->at(resultsi); //results[resultsi].inliers.size();
					float planeWidth = planeResult.width;
					float planeHeight = planeResult.height;
					localFeatureTotal += timer.getElapsed();

					if (featureFilePtr)
					{
						//Header: "frame,plane index,plane type,inlier count,feature label,majority feature,normalX,normalY,normalZ,centerX,centerY,centerZ,width,height,elevation angle,ground height"
						fprintf(featureFilePtr, "%u,%u,%d,%u,%u,%d,%#.6f,%#.6f,%#.6f,%#.6f,%#.6f,%#.6f,%#.6f,%#.6f,%#.6f,%#.6f\n", frame, resultsi, static_cast<int>(planeType), planeInlierCount, featureLabel, featurePlanes.at(resultsi),
							planeNormal.at(0), planeNormal.at(1), planeNormal.at(2), planeCenter.x, planeCenter.y, planeCenter.z, planeWidth, planeHeight, planeElavationAngle, planeGroundHeight);
					}
					else
					{
						//TODO

#if !(defined(DEMO) || defined(TEST_PERFORMANCE))
						const char* planeTypeStr = ((planeType & GROUND) == GROUND) ? "GROUND | SUPPORT" : (planeType == SUPPORT ? "SUPPORT" : "NON-SUPPORT");
						printf("Plane %d: Type-%s, Normal-(%f, %f, %f), Inlier Count-%u, Center-(%f, %f, %f), Width-%f, Height-%f, Elevation Angle-%f, Ground Height-%f\n", resultsi,
							planeTypeStr, planeNormal.at(0), planeNormal.at(1), planeNormal.at(2), planeInlierCount, planeCenter.x, planeCenter.y, planeCenter.z,
							planeWidth, planeHeight, planeElavationAngle, planeGroundHeight);
#endif
					}
				}
            }

#ifdef TEST_PERFORMANCE
			auto planeSearchCPPTotal = _planeFilteringParams.performanceResults->at(0);
			auto planeSearchCTotal = _planeFilteringParams.performanceResults->at(1);
			auto planeSearchC_LoopTotal = _planeFilteringParams.performanceResults->at(2);
			auto internalFeatureSearchTotal = accu(_planeFilteringParams.performanceResults->rows(3, 4));

			printf("Plane Count: %d, Search C: %f, Search C (loop): %f, Search C++: %f, Internal features: %f, Local features: %f\n", 
				results.size(), planeSearchCTotal, planeSearchC_LoopTotal, planeSearchCPPTotal, internalFeatureSearchTotal, localFeatureTotal);
#endif

			if (featureFilePtr)
			{
				fflush(featureFilePtr);
				fclose(featureFilePtr);
			}

			// Set renderer data
            _renderer->setPlaneData(reinterpret_cast<const GLfloat*>(_renderPoints.data()),
                                    reinterpret_cast<const GLfloat*>(_renderColors.data()), currentIndex);
            _renderer->setQuadData(reinterpret_cast<const GLfloat*>(_renderQuads.data()),
                                   reinterpret_cast<const GLfloat*>(_renderQuadColors.data()), quadIndex);
        }
    }
	return true;
}

void Coordinator::computeFromDepthMatrix()
{
    for (int i = 0; i < MaxCount; i++)
    {
        float depth = _depthMatrix->at(i % KinectManager::DEPTH_WIDTH, i / KinectManager::DEPTH_WIDTH);
        //float depth = _depthMatrix->at(i % KinectManager::DEPTH_WIDTH, i / KinectManager::DEPTH_WIDTH);
        _depthBuffer[i] = static_cast<unsigned char>(256 * (1.0f - ((depth - DEPTH_MIN) / float(DEPTH_MAX - DEPTH_MIN))));
    }
}

#define IF_KEY_PRESSED(input) if ((key == input) && action == GLFW_PRESS)
#define IF_KEY_RELEASED(input) if ((key == input) && action == GLFW_RELEASE)
#define IS_MOD_SET(mod_key) if (mods & mod_key) // This doesn't work with "shift" for some reason...

void Coordinator::staticKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    _instance->keyCallback(window, key, scancode, action, mods);
}

void Coordinator::staticMouseCallback(GLFWwindow* window, double xpos, double ypos)
{
    _instance->mouseCallback(window, xpos, ypos);
}

void Coordinator::staticMouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
    _instance->mouseButtonCallback(window, button, action, mods);
}

void Coordinator::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	//TODO: help key

#ifdef PLANE_FILTERING_INTERACTIVE
	IF_KEY_PRESSED(GLFW_KEY_KP_1)
	{
		if (_streaming == false)
		{
			IS_MOD_SET(GLFW_MOD_ALT)
			{
				neh_range -= 1;
			}
			else
			{
				neh_range -= 5;
			}
			IS_MOD_SET(GLFW_MOD_CONTROL)
			{
				printf("Adjusting plane filtering inputs: planeFiltering(%d, %ff, %ff, %ff)\n", neh_range, plane_size, inlier_prec, inlier_dist);
			}
			else
			{
				getData(false, false, true);
			}
		}
	}

	IF_KEY_PRESSED(GLFW_KEY_KP_3)
	{
		if (_streaming == false)
		{
			IS_MOD_SET(GLFW_MOD_ALT)
			{
				neh_range += 1;
			}
			else
			{
				neh_range += 5;
			}
			IS_MOD_SET(GLFW_MOD_CONTROL)
			{
				printf("Adjusting plane filtering inputs: planeFiltering(%d, %ff, %ff, %ff)\n", neh_range, plane_size, inlier_prec, inlier_dist);
			}
			else
			{
				getData(false, false, true);
			}
		}
	}

	IF_KEY_PRESSED(GLFW_KEY_KP_4)
	{
		if (_streaming == false)
		{
			IS_MOD_SET(GLFW_MOD_ALT)
			{
				plane_size -= 1;
			}
			else
			{
				plane_size -= 10;
			}
			IS_MOD_SET(GLFW_MOD_CONTROL)
			{
				printf("Adjusting plane filtering inputs: planeFiltering(%d, %ff, %ff, %ff)\n", neh_range, plane_size, inlier_prec, inlier_dist);
			}
			else
			{
				getData(false, false, true);
			}
		}
	}

	IF_KEY_PRESSED(GLFW_KEY_KP_6)
	{
		if (_streaming == false)
		{
			IS_MOD_SET(GLFW_MOD_ALT)
			{
				plane_size += 1;
			}
			else
			{
				plane_size += 10;
			}
			IS_MOD_SET(GLFW_MOD_CONTROL)
			{
				printf("Adjusting plane filtering inputs: planeFiltering(%d, %ff, %ff, %ff)\n", neh_range, plane_size, inlier_prec, inlier_dist);
			}
			else
			{
				getData(false, false, true);
			}
		}
	}

	IF_KEY_PRESSED(GLFW_KEY_KP_7)
	{
		if (_streaming == false)
		{
			IS_MOD_SET(GLFW_MOD_ALT)
			{
				inlier_prec -= 0.01f;
			}
			else
			{
				inlier_prec -= 0.1f;
			}
			IS_MOD_SET(GLFW_MOD_CONTROL)
			{
				printf("Adjusting plane filtering inputs: planeFiltering(%d, %ff, %ff, %ff)\n", neh_range, plane_size, inlier_prec, inlier_dist);
			}
			else
			{
				getData(false, false, true);
			}
		}
	}

	IF_KEY_PRESSED(GLFW_KEY_KP_9)
	{
		if (_streaming == false)
		{
			IS_MOD_SET(GLFW_MOD_ALT)
			{
				inlier_prec += 0.01f;
			}
			else
			{
				inlier_prec += 0.1f;
			}
			IS_MOD_SET(GLFW_MOD_CONTROL)
			{
				printf("Adjusting plane filtering inputs: planeFiltering(%d, %ff, %ff, %ff)\n", neh_range, plane_size, inlier_prec, inlier_dist);
			}
			else
			{
				getData(false, false, true);
			}
		}
	}

	IF_KEY_PRESSED(GLFW_KEY_KP_8)
	{
		if (_streaming == false)
		{
			IS_MOD_SET(GLFW_MOD_ALT)
			{
				inlier_dist += 0.1f;
			}
			else
			{
				inlier_dist += 1;
			}
			IS_MOD_SET(GLFW_MOD_CONTROL)
			{
				printf("Adjusting plane filtering inputs: planeFiltering(%d, %ff, %ff, %ff)\n", neh_range, plane_size, inlier_prec, inlier_dist);
			}
			else
			{
				getData(false, false, true);
			}
		}
	}

	IF_KEY_PRESSED(GLFW_KEY_KP_2)
	{
		if (_streaming == false)
		{
			IS_MOD_SET(GLFW_MOD_ALT)
			{
				inlier_dist -= 0.1f;
			}
			else
			{
				inlier_dist -= 1;
			}
			IS_MOD_SET(GLFW_MOD_CONTROL)
			{
				printf("Adjusting plane filtering inputs: planeFiltering(%d, %ff, %ff, %ff)\n", neh_range, plane_size, inlier_prec, inlier_dist);
			}
			else
			{
				getData(false, false, true);
			}
		}
	}

#endif

    IF_KEY_PRESSED(GLFW_KEY_Q)
    {
		_renderer->requestWindowClose();
    }

    IF_KEY_PRESSED(GLFW_KEY_0)
    {
        _renderer->setMode(Renderer::Mode::Test);
    }

    IF_KEY_PRESSED(GLFW_KEY_1)
    {
        _renderer->setMode(Renderer::Mode::Depth);
    }

    IF_KEY_PRESSED(GLFW_KEY_2)
    {
        _renderer->setMode(Renderer::Mode::PointCloud);
    }

    IF_KEY_PRESSED(GLFW_KEY_3)
    {
        _renderer->setMode(Renderer::Mode::Planes);
    }

#ifndef DEMO
    IF_KEY_PRESSED(GLFW_KEY_4)
    {
        _renderer->setMode(Renderer::Mode::Quads);
    }
#endif

    IF_KEY_PRESSED(GLFW_KEY_M)
    {
        _streaming = !_streaming;
    }

    IF_KEY_PRESSED(GLFW_KEY_SPACE)
    {
        if (_streaming == false)
        {
            _streaming = true;
            getData(true, true, true);
            _streaming = false;
        }
    }

    IF_KEY_PRESSED(GLFW_KEY_S)
    {
        if (_streaming == false)
        {
            printf("Enter save path (nothing to cancel): ");

            std::string file;
            std::getline(std::cin, file);
            if (file.size() != 0)
            {
				printf("saving\n");
                bool result = _depthMatrix->save(_saveLoadPath + file + ".csv", arma::csv_ascii);
                printf("save %s\n", result ? "was successful" : "failed");
            }
            else
            {
                printf("cancelled\n");
            }
        }
    }

    IF_KEY_PRESSED(GLFW_KEY_L)
    {
        if (_streaming == false)
        {
            printf("Enter load path (nothing to cancel): ");

            std::string file;
            std::getline(std::cin, file);
            if (file.size() != 0)
            {
				printf("loading\n");
                bool result = _depthMatrix->load(_saveLoadPath + file + ".csv", arma::csv_ascii);
				printf("load %s\n", result ? "was successful" : "failed");
                if (result)
                {
                    computeFromDepthMatrix();
                    getData(true, true, true);
                }
            }
            else
            {
                printf("cancelled\n");
            }
        }
    }

	IF_KEY_PRESSED(GLFW_KEY_F)
	{
		if (_streaming == false)
		{
			MatFileManager* mfm = dynamic_cast<MatFileManager*>(_sensor.get());
			if (!mfm)
			{
				printf("Filters can only be used on file based sensors.\n");
			}
			else if (_filterFeature.size())
			{
				printf("Filter set via config. Skipping user input.\n");
			}
			else
			{
				printf("Enter filter (nothing to clear): ");

				bool result;
				std::string filters;
				std::getline(std::cin, filters);
				if (filters.size() != 0)
				{
					printf("applying filter\n");
					result = mfm->filterDepthByLabels(std::vector<std::string>({ filters }), MatFileManager::FILTER_LABEL);
					printf("filter %s\n", result ? "was successful. Reload frame to see results" : "failed");
				}
				else
				{
					result = mfm->clearFilterDepth();
					printf("clearing filter %s\n", result ? "was successful. Reload frame to see results" : "failed");
				}
				if (result)
				{
					uword index = mfm->getCurrentIndex() == 0 ? mfm->getTotalImageCount() - 1 : mfm->getCurrentIndex() - 1;
					mfm->setCurrentIndex(index);
				}
			}
		}
	}

	IF_KEY_PRESSED(GLFW_KEY_C)
	{
		if (_streaming == false)
		{
			MatFileManager* mfm = dynamic_cast<MatFileManager*>(_sensor.get());
			if (!mfm)
			{
				printf("Filter out can only be used on file based sensors.\n");
			}
			else if (_filterFeature.size())
			{
				printf("Filter set via config. Skipping user input.\n");
			}
			else
			{
				printf("Enter filter out (nothing to clear): ");

				bool result;
				std::string filters;
				std::getline(std::cin, filters);
				if (filters.size() != 0)
				{
					printf("applying filter out\n");
					result = mfm->filterDepthByLabels(std::vector<std::string>({ filters }), MatFileManager::FILTER_OUT_LABEL);
					printf("filter out %s\n", result ? "was successful. Reload frame to see results" : "failed");
				}
				else
				{
					result = mfm->clearFilterDepth();
					printf("clearing filter %s\n", result ? "was successful. Reload frame to see results" : "failed");
				}
				if (result)
				{
					uword index = mfm->getCurrentIndex() == 0 ? mfm->getTotalImageCount() - 1 : mfm->getCurrentIndex() - 1;
					mfm->setCurrentIndex(index);
				}
			}
		}
	}

	IF_KEY_PRESSED(GLFW_KEY_T)
	{
		if (_streaming == false)
		{
			MatFileManager* mfm = dynamic_cast<MatFileManager*>(_sensor.get());
			if (!mfm)
			{
				printf("Getting filter results is only avaliable on file based sensors.\n");
			}
			else
			{
				auto labels = mfm->getCurrentLabels();
				if (labels.empty())
				{
					printf("No labels. Make sure you load at least one frame before trying to get labels.\n");
				}
				else
				{
					printf("Current Labels:\n");
					for (const std::string& lab : labels)
					{
						printf(" - %s\n", lab.c_str());
					}
					printf("\n");
				}
			}
		}
	}

    IF_KEY_RELEASED(GLFW_KEY_P)
    {
        _renderer->camera()->setPerspective(_sensor->getFovY(), _renderer->getWidth() / float(_renderer->getHeight()), 0.004f, 4000.0f);
    }

    IF_KEY_RELEASED(GLFW_KEY_O)
    {
        float halfWidth = 4000.0f * tanf(_sensor->getFovX() / 2);
        float halfHeight = 4000.0f * tanf(_sensor->getFovY() / 2);
        _renderer->camera()->setOrtho(-halfWidth, halfWidth, halfHeight, -halfHeight, 0.004f, 4000.0f);
    }

    IF_KEY_PRESSED(GLFW_KEY_R)
    {
        _renderer->camera()->reset();
    }

    IF_KEY_PRESSED(GLFW_KEY_LEFT_SHIFT)
    {
        _shiftDown = true;
    }

    IF_KEY_RELEASED(GLFW_KEY_LEFT_SHIFT)
    {
        _shiftDown = false;
    }
}

void Coordinator::mouseCallback(GLFWwindow* window, double xpos, double ypos)
{
    int width, height;
    glfwGetWindowSize(window, &width, &height);
    if (_leftMouseDown)
    {
        _renderer->camera()->rotate((_startY - ypos) / 4, (_startX - xpos) / 4);
    }
    else if (_rightMouseDown)
    {
        _renderer->camera()->move(xpos - _startX,
                                  _shiftDown ? _startY - ypos : 0,
                                  _shiftDown ? 0 : ypos - _startY);
        _startX = xpos;
        _startY = ypos;
    }
}

void Coordinator::mouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
    if (button == GLFW_MOUSE_BUTTON_LEFT)
    {
        glfwGetCursorPos(window, &_startX, &_startY);
        _leftMouseDown = action == GLFW_PRESS;
        if (_leftMouseDown == false)
        {
            _renderer->camera()->saveRotate();
        }
    }

    if (button == GLFW_MOUSE_BUTTON_RIGHT)
    {
        glfwGetCursorPos(window, &_startX, &_startY);
        _rightMouseDown = action == GLFW_PRESS;
    }
}