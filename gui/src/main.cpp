#include "./Config.h"
#include "./Renderer.h"
#include "./MatFileManager.h"
#include "./Coordinator.h"

#ifdef USE_KINECT
#include "./KinectManager.h"

#ifdef _WIN32
#pragma comment (lib, "Kinect10.lib")
#endif
#endif

#include <memory>
#include <mor.hpp>

std::shared_ptr<mor::ISensor> getSensor(const Config& config)
{
	if (config.getInt("useMatFile"))
	{
		return std::make_shared<MatFileManager>(config);
    }
#ifdef USE_KINECT
	else if (config.getInt("useSimpleSensor"))
	{
		return std::shared_ptr<mor::ISensor>(mor::SensorUtilities::createFakeKinect());
	}
	return std::make_shared<KinectManager>();
#else
	return std::shared_ptr<mor::ISensor>(mor::SensorUtilities::createFakeKinect());
#endif
}

int main()
{
    Config config = Config::fromFile("assets/config.txt");

    // Initialize the sensor
	std::shared_ptr<mor::ISensor> sensor = getSensor(config);
	if (sensor->init() == false)
    {
        return 1;
    }

    // Initialize renderer
	Renderer renderer;
    bool result = renderer.init(config.getInt("windowWidth"), config.getInt("windowHeight"));
    if (result == false)
    {
        return 1;
    }

	sensor->startStreaming();

	Coordinator coordinator(&renderer, sensor, config);
    coordinator.mainLoop();

    sensor->stopStreaming();
    return 0;
}