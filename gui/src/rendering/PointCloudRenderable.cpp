#include "./PointCloudRenderable.h"

bool PointCloudRenderable::init(Camera* camera, int maxPoints, GLuint renderType)
{
    //_program = Program::createProgram("assets/shaders/pointcloud.vert", "assets/shaders/pointcloud.frag");
    _program = Program::createProgram("assets/shaders/pointcloud.vert", "assets/shaders/pointcloud.frag");
    if (_program == nullptr)
    {
        printf("PointCloudRenderable program init failure\n");
        return false;
    }
    _program->setActive();

    _projection = _program->getUniform("Projection");
 
    glGenVertexArrays(1, &_vao);
    glBindVertexArray(_vao);

    glGenBuffers(2, _buffers);

    // set up points buffer
    glBindBuffer(GL_ARRAY_BUFFER, _buffers[POINTS]);
    glBufferData(GL_ARRAY_BUFFER, maxPoints * 3 * sizeof(GLfloat), nullptr, GL_DYNAMIC_DRAW);
    GLuint d_vPosition = _program->getAttribute("vPosition");
    glEnableVertexAttribArray(d_vPosition);
    glVertexAttribPointer(d_vPosition, 3, GL_FLOAT, GL_FALSE, 0, 0);

    // set up color buffer
    glBindBuffer(GL_ARRAY_BUFFER, _buffers[COLORS]);
    glBufferData(GL_ARRAY_BUFFER, maxPoints * 3 * sizeof(GLfloat), nullptr, GL_DYNAMIC_DRAW);
    GLuint d_fColor = _program->getAttribute("vColor");
    printf("vertex location: %d\n", d_vPosition);
    printf("color location: %d\n", d_fColor);
    glEnableVertexAttribArray(d_fColor);
    glVertexAttribPointer(d_fColor, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindVertexArray(0);

    _camera = camera;
    _currentCount = maxPoints;
    _renderType = renderType;

    return true;
}

void PointCloudRenderable::setPointCloudData(const GLfloat* data, const GLfloat* colors, int count)
{
    glBindBuffer(GL_ARRAY_BUFFER, _buffers[POINTS]);
    glBufferSubData(GL_ARRAY_BUFFER, 0, count * 3 * sizeof(GLfloat), data);

    glBindBuffer(GL_ARRAY_BUFFER, _buffers[COLORS]);
    glBufferSubData(GL_ARRAY_BUFFER, 0, count * 3 * sizeof(GLfloat), colors);

    _currentCount = count;
}

void PointCloudRenderable::render()
{
    _program->setActive();
    glBindVertexArray(_vao);

    glUniformMatrix4fv(_projection, 1, GL_FALSE, glm::value_ptr(_camera->matrix()));

    if (_renderType == GL_TRIANGLES)
    {
        //glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); // Get the same result with or without this... it's depreciated, so don't use it (glPolygonMode)
        glDrawArrays(_renderType, 0, _currentCount);
        //glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
    }
    else
    {
        glDrawArrays(_renderType, 0, _currentCount);
    }
}