#include "./TextureRenderable.h"

bool TextureRenderable::init()
{
    typedef glm::highp_vec2 point2;

    // Create program
    _program = Program::createProgram("assets/shaders/texture.vert", "assets/shaders/texture.frag");
    if (_program == nullptr)
    {
        printf("TestRenderable program init failure\n");
        return false;
    }

    glEnable(GL_TEXTURE_2D);

    _program->setActive();

    // init texture data
    glGenTextures(1, &_texture);
    glBindTexture(GL_TEXTURE_2D, _texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    // Create triangle array
    point2 quad[4];
    quad[0] = point2(-1, -1);
    quad[1] = point2(1, -1);
    quad[2] = point2(1, 1);
    quad[3] = point2(-1, 1);

    glGenVertexArrays(1, &_vao);
    glBindVertexArray(_vao);

    GLuint d_triangle;
    glGenBuffers(1, &d_triangle);
    glBindBuffer(GL_ARRAY_BUFFER, d_triangle);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quad), quad, GL_STATIC_DRAW);
    GLuint d_vPosition = _program->getAttribute("vPosition");
    glEnableVertexAttribArray(d_vPosition);
    glVertexAttribPointer(d_vPosition, 2, GL_FLOAT, GL_FALSE, 0, 0);

    glBindVertexArray(0);

    //// generate test texture
    //GLubyte buffer[640 * 480];
    //for (int i = 0; i < 640; i++)
    //{
    //    for (int j = 0; j < 480; j++)
    //    {
    //        buffer[i * 480 + j] = i % 256;
    //    }
    //}

    //setTexture(buffer, 640, 480);

    return true;
}

void TextureRenderable::setTexture(const GLubyte* data, int width, int height)
{
    glBindTexture(GL_TEXTURE_2D, _texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, width, height, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, data);
}

void TextureRenderable::render()
{
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, _texture);

    _program->setActive();
    glBindVertexArray(_vao);
    glDrawArrays(GL_QUADS, 0, 4);
}