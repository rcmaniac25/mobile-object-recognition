#pragma once

#include <GL\glew.h>
#include <glm\vec3.hpp>
#include <glm\gtc\type_ptr.hpp>
#include "./IRenderable.h"
#include "./Program.h"
#include "./Camera.h"

class PlaneRenderable : public IRenderable
{
    bool init(Camera* camera);
    virtual void render();
    void setPlaneData(const float* points, const float* colors, int count);
};