#include "./TestRenderable.h"

bool TestRenderable::init()
{
    typedef glm::highp_vec2 point2;

    // Create program
    _program = Program::createProgram("assets/shaders/vshader.glsl", "assets/shaders/fshader.glsl");
    if (_program == nullptr)
    {   
        printf("TestRenderable program init failure\n");
        return false;
    }

    _program->setActive();

    // Create triangle array
    point2 triangle[3];
    triangle[0] = point2(-1, -1);
    triangle[1] = point2(1, -1);
    triangle[2] = point2(0, 1);

    glGenVertexArrays(1, &_vao);
    glBindVertexArray(_vao);

    GLuint d_triangle;
    glGenBuffers(1, &d_triangle);
    glBindBuffer(GL_ARRAY_BUFFER, d_triangle);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);
    GLuint d_vPosition = _program->getAttribute("vPosition");
    glEnableVertexAttribArray(d_vPosition);
    glVertexAttribPointer(d_vPosition, 2, GL_FLOAT, GL_FALSE, 0, 0);

    glBindVertexArray(0);

    return true;
}

void TestRenderable::render()
{
    _program->setActive();
    glBindVertexArray(_vao);
    glDrawArrays(GL_TRIANGLES, 0, 3);
}