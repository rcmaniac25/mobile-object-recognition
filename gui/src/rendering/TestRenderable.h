#pragma once

#include <GL\glew.h>
#include <glm\vec2.hpp>
#include "./IRenderable.h"
#include "./Program.h"

class TestRenderable : public IRenderable
{
public:
    bool init();
    virtual void render();

private:
    GLuint _vao;
    Program* _program;
};