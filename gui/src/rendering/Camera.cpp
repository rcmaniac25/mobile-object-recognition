#include "./Camera.h"
#include <iostream>

using namespace glm;

Camera::Camera()
    : _matrix(1)
    , _dirty(false)
    , _zoom(0)
    , _startZoom(0)
    , _startRotation(0)
    , _rotation(0)
    , _translation(0)
    , _center(0)
{
}

void Camera::setCenter(float x, float y, float z)
{
    _center = vec3(x, y, z);
    _dirty = true;
}

void Camera::setRotation(float rotationX, float rotationY)
{
    _startRotation = vec2(rotationX, rotationY);
    _dirty = true;
}

void Camera::setStartZoom(float zoom)
{
    _startZoom = zoom;
    _dirty = true;
}

void Camera::move(float x, float y, float z)
{
    _translation += vec3(x, y, z);
    _dirty = true;
}

void Camera::rotate(float rotationX, float rotationY)
{
    _rotation = vec2(rotationX, rotationY);
    _dirty = true;
}

void Camera::saveRotate()
{
    _startRotation += _rotation;
    _rotation = vec2(0);
}

void Camera::zoom(float zoom)
{
    _zoom -= zoom; // bigger zooms should get you closer to the object
    _dirty = true;
}

void Camera::reset()
{
    _startRotation = vec2(0);
    _rotation = vec2(0);
    _translation = vec3(0);
    _zoom = 0;
    _dirty = true;
}

const mat4& Camera::matrix()
{
    if (_dirty)
    {
        vec2 rotation = _rotation + _startRotation;
        //vec3 center = _center + _translation;
        //vec3 eyeOffset = vec3(0, 0, 1);
        //vec3 eyeOffset = vec3(cos(radians(rotation.x)), sin(radians(rotation.x)), 0) * (_startZoom + _zoom);
        //vec3 eyeOffset = vec3(cos(radians(rotation.x)),
        //                      sin(radians(rotation.y)),
        //                      sin(radians(rotation.x)) * cos(radians(rotation.y))) * (_startZoom + _zoom);
        //_matrix = lookAt(center + eyeOffset, center, vec3(0, 1, 0));
        _matrix = glm::translate(glm::rotate(glm::rotate(glm::translate(glm::translate(
            _projection,
            -_center),
            _translation),
            radians(rotation.y), vec3(0, 1, 0)),
            radians(rotation.x), vec3(1, 0, 0)),
            _center);
		_dirty = false;
    }

    //return mat4(1);
    //return lookAt(vec3(1, 0, 2), vec3(1, 0, 0), vec3(0, 1, 0));
    //return _matrix;
    return _matrix;
}

void Camera::setPerspective(float fovy, float aspect, float near, float far)
{
    _projection = glm::perspective(fovy, aspect, near, far);
    _dirty = true;
}

void Camera::setOrtho(float left, float right, float top, float bottom, float near, float far)
{
    _projection = glm::ortho(left, right, bottom, top, near, far);
    _dirty = true;
}
