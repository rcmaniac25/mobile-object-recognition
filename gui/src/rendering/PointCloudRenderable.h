#pragma once

#include <GL\glew.h>
#include <glm\vec3.hpp>
#include <glm\gtc\type_ptr.hpp>
#include "./IRenderable.h"
#include "./Program.h"
#include "./Camera.h"

class PointCloudRenderable : public IRenderable
{
public:
    bool init(Camera* camera, int maxPoints, GLuint renderType);
    virtual void render();
    void setPointCloudData(const GLfloat* data, const GLfloat* colors, int count);

private:
    const int POINTS = 0;
    const int COLORS = 1;

    GLuint _vao;
    Program* _program;
    GLuint _buffers[2];
    GLuint _projection;
    Camera* _camera;
    int _currentCount;
    GLuint _renderType;
};